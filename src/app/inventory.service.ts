import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';
import {ComplexDepreciation, DepreciationItem, InventoryItem} from './models/crms';
import {AccountsResponse, InventoryItemsResponse} from './models/accounts';

@Injectable()
export class InventoryService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createInventoryItem(inventoryItem: InventoryItem): Observable<InventoryItem> {
    return this.http.post<InventoryItem>(environment.baseUrl +
      '/inventory', inventoryItem,  this.authService.createOptions());
  }

  public createDepreciationItem(complexDepreciation: ComplexDepreciation): Observable<InventoryItem> {
    return this.http.post<InventoryItem>(environment.baseUrl +
      '/inventory/amortization', complexDepreciation,  this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/inventory/nextnumber', this.authService.createOptions());
  }

  public getInventoryItems(page: number, size: number): Observable<InventoryItemsResponse> {
    return this.http.get<InventoryItemsResponse>(environment.baseUrl +
      '/inventory?page=' + page + '&size=' + size, this.authService.createOptions());
  }

}
