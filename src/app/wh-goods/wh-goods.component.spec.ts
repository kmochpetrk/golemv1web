import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhGoodsComponent } from './wh-goods.component';

describe('WhGoodsComponent', () => {
  let component: WhGoodsComponent;
  let fixture: ComponentFixture<WhGoodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhGoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhGoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
