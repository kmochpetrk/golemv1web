import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {WgGoodsService} from '../wg-goods.service';
import {WhGoods} from '../models/wh-goods';

@Component({
  selector: 'app-wh-goods',
  templateUrl: './wh-goods.component.html',
  styleUrls: ['./wh-goods.component.css']
})
export class WhGoodsComponent implements OnInit, OnDestroy {
  whGoods: WhGoods[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];
  private sub: Subscription;
  warehouseId: number;
  warehouseName: string;

  constructor(private wgGoodsService: WgGoodsService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.warehouseId = +params.id;
      this.warehouseName = params.warehouse;
      this.wgGoodsService.getWhGoods(this.warehouseId, this.page, this.size).subscribe(whGoodsResponse => {
        this.whGoods = whGoodsResponse.warehouseGoodsServices.content;
        this.page = whGoodsResponse.warehouseGoodsServices.number;
        this.size = whGoodsResponse.warehouseGoodsServices.size;
        this.total = whGoodsResponse.warehouseGoodsServices.totalElements;
        const sizes = Math.ceil(this.total / this.size);
        console.log('sizes ' + sizes);
        const pageNumbers = [];
        if (this.page > 0) {
          pageNumbers.push(this.page);
          // pageNumbers.push(page + 1);
          if (sizes > 1 && (((this.page + 1) < sizes))) {
            pageNumbers.push(this.page + 1);
          }
          // if (this.page < (sizes - 1)) {
          //   pageNumbers.push(page + 2);
          // }
        } else {
          if (sizes >= 0) {
            pageNumbers.push(0);
          }
          if (sizes >= 1) {
            pageNumbers.push(1);
          }
        }
        this.pageNumbers = pageNumbers;
      });
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  // editWarehouse(warehouse: Warehouse) {
  //   this.router.navigate(['/edit-warehouse', warehouse.id]);
  //   this.wgGoodsService.updateWarehouse(warehouse).subscribe((resp) => {
  //     console.log('edit warehouse');
  //     this.ngOnInit();
  //   });
  // }

  addWhGoods() {
    this.router.navigate(['/edit-wh-goods']);
  }

  // deleteWarehouse(warehouse: Warehouse) {
  //   this.warehouseService.deleteWarehouseById(warehouse.id).subscribe((resp) => {
  //     console.log('delete warehouse goods');
  //     this.page = 0;
  //     this.ngOnInit();
  //   });
  // }

  createInputGoodsReceipt() {
    this.router.navigate(['/edit-wh-goods', this.warehouseId, this.warehouseName]);
  }

  createOutputGoodsReceipt() {
    this.router.navigate(['/edit-wh-goods-out', this.warehouseId, this.warehouseName]);
  }

  ngOnDestroy(): void {
  }
}
