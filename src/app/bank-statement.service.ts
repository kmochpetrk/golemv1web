import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {NextNumberResponse} from './models/NextNumberResponse';
import {environment} from '../environments/environment';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {BankStatementRequest} from './models/invoices';

@Injectable()
export class BankStatementService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/bank-statement/nextnumber', this.authService.createOptions());
  }

  public createBankStatement(bankStatementRequest: BankStatementRequest): Observable<any> {
    return this.http.post<any>(environment.baseUrl +
      '/bank-statement', bankStatementRequest,  this.authService.createOptions());
  }
}
