import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankStatementIncomeComponent } from './bank-statement-income.component';

describe('BankStatementIncomeComponent', () => {
  let component: BankStatementIncomeComponent;
  let fixture: ComponentFixture<BankStatementIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankStatementIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankStatementIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
