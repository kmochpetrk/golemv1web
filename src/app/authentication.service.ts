import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs';
import {LoginRequest, LoginResponse} from './models/login';

@Injectable()
export class AuthenticationService {

  token: string;
  issuedAt: Date;

  constructor(private http: HttpClient) { }

  getAccessToken(loginRequest: LoginRequest): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(environment.baseUrl + '/authenticate', loginRequest);
  }

  public createOptions() {
    return {headers: {'content-type': 'application/json', Authorization: 'Bearer ' + this.token}};
  }

  public createOptionsPdf() {
    return {headers: {'content-type': 'application/json', Authorization: 'Bearer ' + this.token}, responseType: 'arraybuffer' as 'json'};
  }

  public createOptionsWithoutAuth() {
    return {headers: {'content-type': 'application/json'}};
  }
}
