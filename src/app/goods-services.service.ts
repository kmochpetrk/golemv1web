import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {GoodsServices, GoodsServicesResponse} from './models/goods-services';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class GoodsServicesService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getGoodsServices(page: number, size: number): Observable<GoodsServicesResponse> {
    return this.http.get<GoodsServicesResponse>(environment.baseUrl +
      '/goodsservices?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public createGoodsServices(goodsServices: GoodsServices): Observable<GoodsServices> {
    return this.http.post<GoodsServices>(environment.baseUrl +
      '/goodsservices', goodsServices, this.authService.createOptions());
  }

  public updateGoodsServices(goodsServices: GoodsServices): Observable<GoodsServices> {
    return this.http.put<GoodsServices>(environment.baseUrl +
      '/goodsservices/' + goodsServices.id, goodsServices, this.authService.createOptions());
  }

  public getGoodsServicesById(id: number): Observable<GoodsServices> {
    return this.http.get<GoodsServices>(environment.baseUrl +
      '/goodsservices/' + id, this.authService.createOptions());
  }

  public deleteGoodsServicesById(id: number): Observable<GoodsServices> {
    return this.http.delete<GoodsServices>(environment.baseUrl +
      '/goodsservices/' + id, this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/goodsservices/nextnumber', this.authService.createOptions());
  }
}
