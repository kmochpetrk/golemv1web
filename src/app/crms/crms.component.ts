import {Component, OnInit} from '@angular/core';
import {GlAccount} from '../models/accounts';
import {Router} from '@angular/router';
import {Crm} from '../models/crms';
import {CrmsService} from '../crms.service';

@Component({
  selector: 'app-crms',
  templateUrl: './crms.component.html',
  styleUrls: ['./crms.component.css']
})
export class CrmsComponent implements OnInit {
  crms: Crm[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  constructor(private crmsService: CrmsService, private router: Router) { }

  ngOnInit(): void {
    this.crmsService.getCrms(this.page, this.size).subscribe(accountsResponse => {
      this.crms = accountsResponse.crms.content;
      this.page = accountsResponse.crms.number;
      this.size = accountsResponse.crms.size;
      this.total = accountsResponse.crms.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  editCrm(crm: Crm) {
    this.router.navigate(['/edit-crm', crm.id]);
    // this.crmsService.updateCrm(crm).subscribe((resp) => {
    //   console.log('edit crm');
    //   this.ngOnInit();
    // });
  }

  createCrm() {
    this.router.navigate(['/edit-crm']);
  }

  deleteCrm(crm: Crm) {
    this.crmsService.deleteCrmById(crm.id).subscribe((resp) => {
      console.log('delete crm');
      this.page = 0;
      this.ngOnInit();
    });
  }
}
