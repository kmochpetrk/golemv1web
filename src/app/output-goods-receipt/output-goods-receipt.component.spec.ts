import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputGoodsReceiptComponent } from './output-goods-receipt.component';

describe('OutputGoodsReceiptComponent', () => {
  let component: OutputGoodsReceiptComponent;
  let fixture: ComponentFixture<OutputGoodsReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputGoodsReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputGoodsReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
