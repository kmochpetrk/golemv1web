import {Component, OnInit} from '@angular/core';
import {CrmItem, GlWriteFe, GlWriteFeBoth, GsItem} from '../models/gl-writes';
import {Booking, GlAccount} from '../models/accounts';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GoodsServices} from '../models/goods-services';
import {formatDate} from '@angular/common';
import {AccountService} from '../account.service';
import {GoodsServicesService} from '../goods-services.service';
import {CrmsService} from '../crms.service';
import {
  GeneralAccountingPostRequest,
  GeneralAccountingPredictRequest,
  GeneralAccountingTrainRequest,
  Rule,
  RuleElement
} from '../models/general-accounting-train-request';
import {GlWriteDto, WarehouseGoodsDto} from '../models/InputGoodsReceipt';
import {BookingsService} from '../bookings.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gl-booking',
  templateUrl: './gl-booking.component.html',
  styleUrls: ['./gl-booking.component.css']
})
export class GlBookingComponent implements OnInit {

  rows: Array<GlWriteFeBoth> = [];

  savedBooking: Booking;

  accountingForm: FormGroup;

  identNext = 0;
  identGsNext = 0;

  keywords = [];
  decided = false;

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];
  selectedRow: GlWriteFeBoth;
  documentTypes = ['INPUT_INVOICE', 'OUTPUT_INVOICE', 'CASH_OUTPUT_RECEIPT', 'CASH_INPUT_RECEIPT', 'GOODS_INPUT_RECEIPT',
    'GOODS_OUTPUT_RECEIPT', 'BANK_STATEMENT', 'INTERNAL'];

  goodsServices: Array<GoodsServices> = [];

  gsItems: Array<GsItem> = [];

  crm: CrmItem[];

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private goodsService: GoodsServicesService,
              private crmService: CrmsService, private bookingsService: BookingsService, private router: Router) {
    this.accountingForm = this.formBuilder.group({
      documentType: ['', [Validators.required]],
      description: ['', [Validators.required]],
      vatDate: ['', [Validators.required]],
      payDate: ['', [Validators.required]],
      issueDate: ['', [Validators.required]],
      crmCompany: ['', [Validators.required]],
      variableSymbol: ['', []]
    });
  }

  ngOnInit(): void {
    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.accountsDebit = accounts.accounts.content;
      this.accountsCredit = accounts.accounts.content;
    });
    this.goodsService.getGoodsServices(0, 100).subscribe(goodsServices => {
      this.goodsServices = goodsServices.goodsServices.content;
    });
    // const accountNull = new GlAccount();
    // accountNull.acctNo = '';
    // accountNull.description = '';
    // const accountDebit = new GlAccount();
    // accountDebit.acctNo = '121100';
    // accountDebit.glAccountSideType = 'DEBIT';
    // accountDebit.description = 'common bank account';
    // const accountCredit = new GlAccount();
    // accountCredit.acctNo = '601100';
    // accountCredit.glAccountSideType = 'DEBIT';
    // accountCredit.description = 'Income from goods';
    // this.accountsDebit = [accountNull, accountDebit, accountCredit];
    // this.accountsCredit = [accountNull, accountDebit, accountCredit];



    // const goodsServices1 = new GoodsServices();
    // goodsServices1.id = 101;
    // goodsServices1.vatPercents = 15;
    // goodsServices1.numberOfUnitsInPacket = 1;
    // goodsServices1.packetOutputPrice = 10;
    // goodsServices1.packetInputPrice = 8;
    // goodsServices1.name = 'Tesarske prace';
    // goodsServices1.unit = 'hour';
    // this.goodsServices.push(goodsServices1);
    //
    // const goodsServices2 = new GoodsServices();
    // goodsServices2.id = 102;
    // goodsServices2.vatPercents = 19;
    // goodsServices2.numberOfUnitsInPacket = 1;
    // goodsServices2.packetOutputPrice = 15;
    // goodsServices2.packetInputPrice = 12;
    // goodsServices2.name = 'Zednicke prace';
    // goodsServices2.unit = 'hour';
    // this.goodsServices.push(goodsServices2);

    this.accountingForm.get('issueDate').setValue(formatDate(new Date(), 'yyyy-MM-dd', 'en'), {onlySelf: true});

    // this.crm = [{id: 1, name: 'Adidas'}, {id: 2, name: 'Puma'}, {id: 3, name: 'Botas'}];

    this.crmService.getCrms(0, 1000).subscribe(data => {
      this.crm = data.crms.content;
    });
  }

  addRow() {
    const newRow = new GlWriteFeBoth();
    newRow.ident = this.identNext++;
    newRow.debit = new GlWriteFe();

    newRow.credit = new GlWriteFe();
    this.rows.push(newRow);
  }

  deleteRow() {
    if (this.selectedRow) {
      this.rows = this.rows.filter(row => row.ident !== this.selectedRow.ident);
      this.selectedRow = null;
    }
  }

  selectRow(glWrite: GlWriteFeBoth) {
    if (this.selectedRow && this.selectedRow.ident === glWrite.ident) {
      this.selectedRow = null;
    } else {
      this.selectedRow = glWrite;
    }
  }

  changeDocumentType(e) {
    this.documentType.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get documentType() {
    return this.accountingForm.get('documentType');
  }

  submitAccountingForm() {
  }

  blurDesc($event: FocusEvent) {
    console.log('FocusEventType: ' + $event.type);
    console.log('FocusEvent: ' + this.accountingForm.get('description').value);
  }

  addRowGs() {
    const gsItem = new GsItem();
    gsItem.ident = this.identGsNext++;
    this.gsItems.push(gsItem);
  }

  calcPrice(rowsElement: GsItem): number {
    const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
    if (goodsServices1.length === 1 && rowsElement.count) {
      return (this.isInputPrice() ? goodsServices1[0].packetInputPrice : goodsServices1[0].packetOutputPrice) * rowsElement.count;
    } else {
      return 0;
    }
  }

  calcVat(rowsElement: GsItem): number {
    const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
    if (goodsServices1.length === 1 && rowsElement.count) {
      return Math.round((this.isInputPrice() ? goodsServices1[0].packetInputPrice : goodsServices1[0].packetOutputPrice) *
        rowsElement.count * (goodsServices1[0].vatPercents / 100) * 100) / 100;
    } else {
      return 0;
    }
  }

  calcTotal(rowsElement: GsItem) {
    const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
    if (goodsServices1.length === 1 && rowsElement.count) {
      return Math.round((this.isInputPrice() ? goodsServices1[0].packetInputPrice :
        goodsServices1[0].packetOutputPrice) * rowsElement.count *
        ((100 + goodsServices1[0].vatPercents) / 100) * 100) / 100;
    } else {
      return 0;
    }
  }

  calcTotalPrice(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.calcPrice(item);
    });
    return total;
  }

  calcTotalVat(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.calcVat(item);
    });
    return total;
  }

  calcTotalTotal(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.calcTotal(item);
    });
    return total;
  }

  delRowGs(rowsElement: GsItem) {
    this.gsItems = this.gsItems.filter(elem => elem.ident !== rowsElement.ident);
  }

  changeCompany(e) {
    this.crmCompany.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get crmCompany() {
    return this.accountingForm.get('crmCompany');
  }

  onEnter(e) {
    console.log('keyup enter:' + e.target.value);
    if (this.rows.length === 0) {
      // predict
      console.log('predict');
      const generalAccountingPredictRequest = new GeneralAccountingPredictRequest();
      generalAccountingPredictRequest.sentence = e.target.value;
      generalAccountingPredictRequest.glWholeWriteType = this.accountingForm.get('documentType').value.split(':')[1].trim();
      this.accountService.postPredict(generalAccountingPredictRequest).subscribe(resp => {
        console.log('resp predict: ' + JSON.stringify(resp.rule.glWholeWriteType));
        resp.rule.ruleElements.forEach(re => {
          const gt = re.accountGroupingType;
          this.accountService.getAccountsByGroup(gt).subscribe(gtResponse  => {
            if (re.glAccountSideType === 'DEBIT') {
              if (this.identNext === 0 || this.rows[this.identNext - 1].debit.acctNoWithDesc != null) {
                this.addRow();
              }
              this.rows[this.identNext - 1].debit.acctNoWithDesc = gtResponse[0].acctNo + '-' + gtResponse[0].description;
              this.rows[this.identNext - 1].debit.multiplier = re.percents;
            }
            if (re.glAccountSideType === 'CREDIT') {
              if (this.identNext === 0 || this.rows[this.identNext - 1].credit.acctNoWithDesc != null) {
                this.addRow();
              }
              this.rows[this.identNext - 1].credit.acctNoWithDesc = gtResponse[0].acctNo + '-' + gtResponse[0].description;
              this.rows[this.identNext - 1].credit.multiplier = re.percents;
            }
          });
        });
      });


    } else {
      // train
      console.log('train');
      const generalAccountingTrainRequest = new GeneralAccountingTrainRequest();
      generalAccountingTrainRequest.sentence = e.target.value;
      generalAccountingTrainRequest.glWholeWriteType = this.accountingForm.get('documentType').value.split(':')[1].trim();
      generalAccountingTrainRequest.rule = new Rule();
      generalAccountingTrainRequest.rule.glWholeWriteType = generalAccountingTrainRequest.glWholeWriteType;
      generalAccountingTrainRequest.rule.ruleElements = [];
      this.rows.forEach(rowOne => {
        if (rowOne.debit.acctNoWithDesc) {
          const reDebit = new RuleElement();
          reDebit.glAccountSideType = 'DEBIT';
          const debitGlAccount = this.findAccountByAcctno(rowOne.debit.acctNoWithDesc, this.accountsDebit);
          reDebit.accountGroupingType = debitGlAccount.glAccountGroupingType;
          reDebit.percents = rowOne.debit.amount;
          generalAccountingTrainRequest.rule.ruleElements.push(reDebit);
        }
        if (rowOne.credit.acctNoWithDesc) {
          const reCredit = new RuleElement();
          reCredit.glAccountSideType = 'CREDIT';
          const creditGlAccount = this.findAccountByAcctno(rowOne.credit.acctNoWithDesc, this.accountsCredit);
          reCredit.accountGroupingType = creditGlAccount.glAccountGroupingType;
          reCredit.percents = rowOne.credit.amount;
          generalAccountingTrainRequest.rule.ruleElements.push(reCredit);
        }
      });
      this.accountService.postTrain(generalAccountingTrainRequest).subscribe(resp => {
        console.log('resp ' + JSON.stringify(resp.sentenceSaved));
      });
    }
    // this.keywords.push(e.target.value.trim());
    // e.target.value = '';
    // this.evaluateBooking();
  }

  private evaluateBooking() { // todo call server
    if (this.keywords.includes('rent') && this.keywords.includes('for') && this.keywords.includes('car') && this.keywords.length === 3) {
      this.decided = true;
      this.rows = [];
      this.addRow();
      this.rows[0].debit = {acctNoWithDesc: '518100-rent for cars', analyticPart: '300', amount: 1000, state: 'new'};
      this.rows[0].credit = {acctNoWithDesc: '321100-suppliers', analyticPart: '800', amount: 1000, state: 'new'};
      this.addRow();
      this.rows[1].debit = {acctNoWithDesc: '343100-vat', analyticPart: '100', amount: 190, state: 'new'};
    } else {
      this.decided = false;
      this.rows = [];
    }

    if (this.keywords.includes('rent') && this.keywords.includes('for') && this.keywords.length === 2) {
      this.renderVariants(['car', 'office']);
    }
  }

  private renderVariants(variants: string[]) {
    this.keywords.push('(');
    variants.forEach((kw, i) => {
      this.keywords.push(kw);
      if (i !== (variants.length - 1)) {
        // last one
        this.keywords.push(',');
      }
    });
    this.keywords.push(')');
  }

  removeKeyword(keyword: string) {
    this.keywords = this.keywords.filter(keyw => keyw !== keyword);
    this.evaluateBooking();
  }

  bcKeywords() {
    return this.decided ? 'lightgreen' : 'lightblue';
  }

  findAccountByAcctno(acctnoWithDesc: string, accounts: Array<GlAccount>): GlAccount {
    const strings = acctnoWithDesc.split('-');
    const acctno = strings[0].trim();
    const glAccounts = accounts.filter(account => account.acctNo === acctno);
    return glAccounts[0];
  }

  doPosting() {
    // do posting
    console.log('do posting');
    const generalAccountingPostingRequest = new GeneralAccountingPostRequest();
    // generalAccountingPostingRequest.sentence = e.target.value;
    generalAccountingPostingRequest.glWholeWriteType = this.accountingForm.get('documentType').value.split(':')[1].trim();
    generalAccountingPostingRequest.issueDate = this.accountingForm.get('issueDate').value;
    generalAccountingPostingRequest.vatDate = this.accountingForm.get('vatDate').value;
    generalAccountingPostingRequest.payDate = this.accountingForm.get('payDate').value;
    generalAccountingPostingRequest.description = this.accountingForm.get('description').value;
    generalAccountingPostingRequest.variableSymbol = this.accountingForm.get('variableSymbol').value;
    if (this.accountingForm.get('crmCompany').value) {
      generalAccountingPostingRequest.crmId = this.accountingForm.get('crmCompany').value.split(':')[1].trim();
    }
    generalAccountingPostingRequest.glWrites = [];
    generalAccountingPostingRequest.warehouseGoodsDtos = [];
    this.rows.forEach(rowOne => {
      if (rowOne.debit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        // glWriteDto.glAccountSideType = 'DEBIT';
        const debitGlAccount = this.findAccountByAcctno(rowOne.debit.acctNoWithDesc, this.accountsDebit);
        glWriteDto.glAccountSideType = 'DEBIT';
        glWriteDto.amount = rowOne.debit.amount;
        glWriteDto.acctNo = debitGlAccount.acctNo;
        glWriteDto.glAccountId = debitGlAccount.id;
        generalAccountingPostingRequest.glWrites.push(glWriteDto);
      }
      if (rowOne.credit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        // glWriteDto.glAccountSideType = 'CREDIT';
        const creditGlAccount = this.findAccountByAcctno(rowOne.credit.acctNoWithDesc, this.accountsCredit);
        glWriteDto.glAccountSideType = 'CREDIT';
        glWriteDto.glAccountId = creditGlAccount.id;
        glWriteDto.acctNo = creditGlAccount.acctNo;
        glWriteDto.amount = rowOne.credit.amount;
        generalAccountingPostingRequest.glWrites.push(glWriteDto);
      }
    });
    this.gsItems.forEach(gsItem => {
      const warehouseGoodsDto = new WarehouseGoodsDto();
      warehouseGoodsDto.count = gsItem.count;
      warehouseGoodsDto.goodsServiceId = gsItem.id;
      warehouseGoodsDto.unitPrice = this.getUnitPrice(gsItem.id);
      warehouseGoodsDto.vatTotal = this.getVat(gsItem.id, gsItem.count);
      warehouseGoodsDto.priceTotal = this.getPrice(gsItem.id, gsItem.count);
      generalAccountingPostingRequest.warehouseGoodsDtos.push(warehouseGoodsDto);
    });
    console.log(JSON.stringify(generalAccountingPostingRequest));
    this.bookingsService.createBooking(generalAccountingPostingRequest).subscribe(resp => {
      console.log('resp ' + JSON.stringify(resp));
      this.savedBooking = resp;
      this.router.navigate(['/menu']);
    });
  }

  getUnitPrice(id: number): number {
    if (id) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return (this.isInputPrice() ? whGoodsLocal[0].packetInputPrice : whGoodsLocal[0].packetOutputPrice);
    }
    return 0;
  }

  getVat(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round((((whGoodsLocal[0].vatPercents / 100) *
        (this.isInputPrice() ? whGoodsLocal[0].packetInputPrice : whGoodsLocal[0].packetOutputPrice)) * count) * 100) / 100;
    }
    return 0;
  }

  getPrice(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round(((this.isInputPrice() ? whGoodsLocal[0].packetInputPrice : whGoodsLocal[0].packetOutputPrice) * count +
        this.getVat(id, count)) * 100) / 100;
    }
    return 0;
  }

  isInputPrice(): boolean {
    const inputType = this.accountingForm.get('documentType').value.split(':')[1].trim();
    if (inputType === 'OUTPUT_INVOICE') {
      return false;
    }
    return true;
  }
}
