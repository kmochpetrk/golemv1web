import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlBookingComponent } from './gl-booking.component';

describe('GlBookingComponent', () => {
  let component: GlBookingComponent;
  let fixture: ComponentFixture<GlBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
