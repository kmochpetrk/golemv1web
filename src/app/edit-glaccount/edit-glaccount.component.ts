import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../account.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GlAccount} from '../models/accounts';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-edit-glaccount',
  templateUrl: './edit-glaccount.component.html',
  styleUrls: ['./edit-glaccount.component.css']
})
export class EditGlaccountComponent implements OnInit, OnDestroy {
  glAccountTypes = ['PROFIT_LOSS', 'BALANCE', 'CALCULATED'];
  glAccountSideTypes = ['DEBIT', 'CREDIT'];
  editGlAccountForm: FormGroup;

  readAccount: GlAccount;

  groupPlDeb;
  groupPlCred;
  groupBalDeb;
  groupBalCred;
  groupCalDeb;
  groupCalCred;
  groupModel = [];


  private sub: Subscription;
  id: number;

  constructor(private formBuilder: FormBuilder, public activatedRoute: ActivatedRoute,
              private accountsService: AccountService, private router: Router) {
    this.editGlAccountForm = this.formBuilder.group({
      acctNo: ['', [Validators.required]],
      description: ['', [Validators.required]],
      glAccountType: ['', [Validators.required]],
      glAccountSideType: ['', [Validators.required]],
      glAccountGroup: ['', [Validators.required]],
      additionalInfo: ['']
    });
    this.editGlAccountForm.get('glAccountType').valueChanges.subscribe((val) => {
      this.initData(false);
    });
    this.editGlAccountForm.get('glAccountSideType').valueChanges.subscribe((val) => {
      this.initData(false);
    });
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params.id;
      if (this.id) {
        this.accountsService.getAccountById(this.id).subscribe((glAccount) => {
          this.readAccount = glAccount;
          this.editGlAccountForm.get('acctNo').setValue(glAccount.acctNo, {
            onlySelf: true, emitEvent: false
          });
          this.editGlAccountForm.get('description').setValue(glAccount.description, {
            onlySelf: true, emitEvent: false
          });
          this.editGlAccountForm.get('additionalInfo').setValue(glAccount.additionalInfo, {
            onlySelf: true, emitEvent: false
          });
          const glAccountType = this.glAccountTypes.indexOf(glAccount.glAccountType) + ': ' + glAccount.glAccountType;
          this.editGlAccountForm.get('glAccountType').setValue(glAccountType, {
            onlySelf: true, emitEvent: false
          });
          const glAccountSideType = this.glAccountSideTypes.indexOf(glAccount.glAccountSideType) + ': ' + glAccount.glAccountSideType;
          this.editGlAccountForm.get('glAccountSideType').setValue(glAccountSideType, {
            onlySelf: true, emitEvent: false
          });
          this.initData();
        });
      } else {
        this.editGlAccountForm.get('glAccountType').setValue('1: BALANCE', {
          onlySelf: true, emitEvent: false
        });
        this.editGlAccountForm.get('glAccountSideType').setValue('0: DEBIT', {
          onlySelf: true, emitEvent: false
        });
      }
      this.initData();
    });
    this.accountsService.getGroups('PROFIT_LOSS', 'DEBIT').subscribe(resp1 => {
      this.groupPlDeb = resp1;
      this.initData();
    });
    this.accountsService.getGroups('PROFIT_LOSS', 'CREDIT').subscribe(resp2 => {
      this.groupPlCred = resp2;
      this.initData();
    });
    this.accountsService.getGroups('BALANCE', 'DEBIT').subscribe(resp3 => {
      this.groupBalDeb = resp3;
      this.initData();
    });
    this.accountsService.getGroups('BALANCE', 'CREDIT').subscribe(resp4 => {
      this.groupBalCred = resp4;
      this.initData();
    });
    this.accountsService.getGroups('CALCULATED', 'DEBIT').subscribe(resp3 => {
      this.groupCalDeb = resp3;
      this.initData();
    });
    this.accountsService.getGroups('CALCULATED', 'CREDIT').subscribe(resp4 => {
      this.groupCalCred = resp4;
      this.initData();
    });
  }

  initData(init = true) {
    if (this.groupPlCred && this.groupPlDeb && this.groupBalCred && this.groupBalDeb) {
      if (this.editGlAccountForm.get('glAccountType').value && this.editGlAccountForm.get('glAccountSideType').value) {
        if (this.editGlAccountForm.get('glAccountType').value === '0: PROFIT_LOSS' &&
          this.editGlAccountForm.get('glAccountSideType').value === '0: DEBIT') {
          this.groupModel = this.groupPlDeb;
        }
        if (this.editGlAccountForm.get('glAccountType').value === '0: PROFIT_LOSS' &&
          this.editGlAccountForm.get('glAccountSideType').value === '1: CREDIT') {
          this.groupModel = this.groupPlCred;
        }
        if (this.editGlAccountForm.get('glAccountType').value === '1: BALANCE' &&
          this.editGlAccountForm.get('glAccountSideType').value === '0: DEBIT') {
          this.groupModel = this.groupBalDeb;
        }
        if (this.editGlAccountForm.get('glAccountType').value === '1: BALANCE' &&
          this.editGlAccountForm.get('glAccountSideType').value === '1: CREDIT') {
          this.groupModel = this.groupBalCred;
        }
        if (this.editGlAccountForm.get('glAccountType').value === '2: CALCULATED' &&
          this.editGlAccountForm.get('glAccountSideType').value === '0: DEBIT') {
          this.groupModel = this.groupCalDeb;
        }
        if (this.editGlAccountForm.get('glAccountType').value === '2: CALCULATED' &&
          this.editGlAccountForm.get('glAccountSideType').value === '1: CREDIT') {
          this.groupModel = this.groupCalCred;
        }
        if (this.readAccount && this.readAccount.glAccountGroupingType) {
          const glAccountGroup = this.groupModel.indexOf(this.readAccount.glAccountGroupingType) +
            ': ' + this.readAccount.glAccountGroupingType;
          this.editGlAccountForm.get('glAccountGroup').setValue(glAccountGroup, {
            onlySelf: true, emitEvent: false
          });
        }
        if (!init) {
          let glAccountGroup = String(this.groupModel.indexOf(this.editGlAccountForm.get('glAccountGroup').value));
          if (glAccountGroup === '-1') {
            glAccountGroup = '0: ' + this.groupModel[0];
          } else {
            glAccountGroup = glAccountGroup + ': ' + this.editGlAccountForm.get('glAccountGroup').value;
          }
          this.editGlAccountForm.get('glAccountGroup').setValue(glAccountGroup, {
            onlySelf: true, emitEvent: false
          });
        }
      }
    }
  }

  submitGlAccountForm() {
    this.editGlAccountForm.updateValueAndValidity();
    if (this.editGlAccountForm.valid) {
      const glAccount = new GlAccount();
      if (this.id) {
        glAccount.id = this.id;
      }
      glAccount.acctNo = this.editGlAccountForm.get('acctNo').value;
      glAccount.description = this.editGlAccountForm.get('description').value;
      glAccount.glAccountType = this.editGlAccountForm.get('glAccountType').value.split(':')[1].trim();
      glAccount.glAccountSideType = this.editGlAccountForm.get('glAccountSideType').value.split(':')[1].trim();
      glAccount.glAccountGroupingType = this.editGlAccountForm.get('glAccountGroup').value.split(':')[1].trim();
      glAccount.additionalInfo = this.editGlAccountForm.get('additionalInfo').value;
      if (!this.id) {
        this.accountsService.createAccount(glAccount).subscribe((resp) => {
          console.log('Account saved.');
          this.router.navigate(['/glaccounts']);
        });
      } else {
        this.accountsService.updateAccount(glAccount).subscribe((resp) => {
          console.log('Account saved.');
          this.router.navigate(['/glaccounts']);
        });
      }
    }
  }

  changeGlAccountType(e) {
    this.glAccountType.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get glAccountType() {
    return this.editGlAccountForm.get('glAccountType');
  }

  changeGlAccountSideType(e) {
    this.glAccountSideType.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get glAccountSideType() {
    return this.editGlAccountForm.get('glAccountSideType');
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  get glAccountGroup() {
    return this.editGlAccountForm.get('glAccountGroup');
  }


  changeGlAccountGroup(e) {
    this.glAccountGroup.setValue(e.target.value, {
      onlySelf: true
    });
  }
}
