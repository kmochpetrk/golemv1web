import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {WarehouseService} from '../warehouse.service';
import {Warehouse} from '../models/warehouses';

@Component({
  selector: 'app-warehouses',
  templateUrl: './warehouses.component.html',
  styleUrls: ['./warehouses.component.css']
})
export class WarehousesComponent implements OnInit {
  warehouses: Warehouse[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  constructor(private warehouseService: WarehouseService, private router: Router) { }

  ngOnInit(): void {
    this.warehouseService.getWarehouses(this.page, this.size).subscribe(warehousesResponse => {
      this.warehouses = warehousesResponse.warehouses.content;
      this.page = warehousesResponse.warehouses.number;
      this.size = warehousesResponse.warehouses.size;
      this.total = warehousesResponse.warehouses.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  editWarehouse(warehouse: Warehouse) {
    this.router.navigate(['/edit-warehouse', warehouse.id]);
    this.warehouseService.updateWarehouse(warehouse).subscribe((resp) => {
      console.log('edit warehouse');
      this.ngOnInit();
    });
  }

  createWarehouse() {
    this.router.navigate(['/edit-warehouse']);
  }

  deleteWarehouse(warehouse: Warehouse) {
    this.warehouseService.deleteWarehouseById(warehouse.id).subscribe((resp) => {
      console.log('delete warehouse');
      this.page = 0;
      this.ngOnInit();
    });
  }

  show(warehouse: Warehouse) {
    this.router.navigate(['/warehouse-goods', warehouse.id, warehouse.name]);
  }
}
