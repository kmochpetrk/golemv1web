import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreateCompanyRooUserRequest, CreateCompanyRooUserResponse, LoginRequest, LoginResponse} from './models/login';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';

@Injectable()
export class CreateCompanyService {

  constructor(private http: HttpClient) { }

  createCompanyRootUser(createCompanyRooUserRequest: CreateCompanyRooUserRequest): Observable<CreateCompanyRooUserResponse> {
    return this.http.post<CreateCompanyRooUserResponse>(environment.baseUrl + '/create-company', createCompanyRooUserRequest,
      this.createOptionsWithoutAuth());
  }

  public createOptionsWithoutAuth() {
    return {headers: {'content-type': 'application/json'}};
  }
}
