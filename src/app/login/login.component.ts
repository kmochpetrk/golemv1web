import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';
import {LoginRequest} from '../models/login';
import {AccountService} from '../account.service';
import {Router} from '@angular/router';
import {EshopService} from '../eshop/eshop.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  token: string;
  numberOfAccounts: number;
  eshops: Array<string>;

  constructor(private formBuilder: FormBuilder, public authenticationService: AuthenticationService,
              private accountsService: AccountService, private router: Router, private eshopService: EshopService) {
    this.loginForm = this.formBuilder.group({
      company: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.eshopService.getCompanies().subscribe(resp => {
      this.eshops = resp.companies;
    });
  }

  submitLoginForm() {
    if (this.loginForm.valid) {
      const loginRequest = new LoginRequest();

      loginRequest.username = this.loginForm.get('company').value + ':' + this.loginForm.get('username').value;
      loginRequest.password = this.loginForm.get('password').value;
      this.authenticationService.getAccessToken(loginRequest).subscribe(loginResponse => {
        this.token = loginResponse.jwtToken;
        this.authenticationService.token = loginResponse.jwtToken;
        this.authenticationService.issuedAt = new Date();
        // this.accountsService.getAccounts().subscribe(accountsResponse => {
        //   this.numberOfAccounts = accountsResponse.accounts.content.length;
        // });
        this.router.navigate(['menu']);
      });
    }
  }

  createCompanyWithRootUser() {
    this.router.navigate(['create-company']);
  }
}
