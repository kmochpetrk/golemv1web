import {Component, OnInit} from '@angular/core';
import {InputCashReceiptRequest, InputGoodsReceipt, OutputGoodsReceipt} from '../models/invoices';
import {GlWriteFe, GlWriteFeBoth, GsItem} from '../models/gl-writes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlAccount} from '../models/accounts';
import {GoodsServices} from '../models/goods-services';
import {AccountService} from '../account.service';
import {GoodsServicesService} from '../goods-services.service';
import {CrmsService} from '../crms.service';
import {ActivatedRoute, Router} from '@angular/router';
import {formatDate} from '@angular/common';
import {GlWriteDto, WarehouseGoodsDto} from '../models/InputGoodsReceipt';
import {InputCashReceiptService} from '../input-cash-receipt.service';
import {OutputInvoiceService} from '../output-invoice.service';

@Component({
  selector: 'app-input-cash-receipt',
  templateUrl: './input-cash-receipt.component.html',
  styleUrls: ['./input-cash-receipt.component.css']
})
export class InputCashReceiptComponent implements OnInit {

  inputGoodsReceipts: InputGoodsReceipt[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  griIds: Array<number> = [];

  rows: Array<GlWriteFeBoth> = [];

  accountingForm: FormGroup;

  savedId: number;

  identNext = 0;
  identGsNext = 0;

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];
  selectedRow: GlWriteFeBoth;
  // documentTypes=['InputInvoice', 'OutputInvoice', 'cash_output_receipt', 'cash_input_receipt', 'Warehouse_Income', 'Warehouse_Outcome'];

  goodsServices: Array<GoodsServices> = [];

  gsItems: Array<GsItem> = [];
  cashiers = [];

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private goodsService: GoodsServicesService,
              private crmService: CrmsService, private activatedRoute: ActivatedRoute, private router: Router,
              private inputCashReceiptService: InputCashReceiptService, private outputInvoiceService: OutputInvoiceService) {
    this.accountingForm = this.formBuilder.group({
      issueDate: ['', [Validators.required]],
      documentNumber: ['', [Validators.required]],
      cashierTitle: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.goodsService.getGoodsServices(0, 100).subscribe(goodsServices => {
      this.goodsServices = goodsServices.goodsServices.content;
    });
    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.accountsDebit = accounts.accounts.content;
      this.accountsCredit = accounts.accounts.content;
      this.cashiers = accounts.accounts.content.filter(acc =>
        acc.glAccountGroupingType === 'CASH_AND_EQUIVALENTS').map(u => u.additionalInfo);
    });
    this.accountingForm.get('issueDate').setValue(formatDate(new Date(), 'yyyy-MM-dd', 'en'), {onlySelf: true});
    this.inputCashReceiptService.nextDocumentNumber().subscribe(resp => {
      this.accountingForm.get('documentNumber').setValue(resp.documentNumber, {
        onlySelf: true, emitEvent: false
      });
    });
    this.readDataOutReceipt();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  addRow() {
    const newRow = new GlWriteFeBoth();
    newRow.ident = this.identNext++;
    newRow.debit = new GlWriteFe();

    newRow.credit = new GlWriteFe();
    this.rows.push(newRow);
  }

  deleteRow() {
    if (this.selectedRow) {
      this.rows = this.rows.filter(row => row.ident !== this.selectedRow.ident);
      this.selectedRow = null;
    }
  }

  selectRow(glWrite: GlWriteFeBoth) {
    if (this.selectedRow && this.selectedRow.ident === glWrite.ident) {
      this.selectedRow = null;
    } else {
      this.selectedRow = glWrite;
    }
  }

  submitAccountingForm() {
    const request = new InputCashReceiptRequest();
    request.issueDate = this.accountingForm.get('issueDate').value;
    request.cashierTitle = this.accountingForm.get('cashierTitle').value.split(':')[1];
    request.gorIds = this.griIds;
    request.documentNumber = this.accountingForm.get('documentNumber').value;
    const glWrites = [];
    const goods = [];
    this.rows.forEach(row => {
      if (row.debit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'DEBIT';
        glWriteDto.amount = row.debit.amount;
        glWriteDto.analyticPart = row.debit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.debit.acctNoWithDesc, this.accountsDebit).id;
        glWrites.push(glWriteDto);
      }
      if (row.credit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'CREDIT';
        glWriteDto.amount = row.credit.amount;
        glWriteDto.analyticPart = row.credit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.credit.acctNoWithDesc, this.accountsCredit).id;
        glWrites.push(glWriteDto);
      }
    });
    this.gsItems.forEach(gsItem => {
      const warehouseGoodsDto = new WarehouseGoodsDto();
      warehouseGoodsDto.count = gsItem.count;
      warehouseGoodsDto.goodsServiceId = gsItem.id;
      warehouseGoodsDto.unitPrice = this.getUnitPrice(gsItem.id);
      warehouseGoodsDto.vatTotal = this.getVat(gsItem.id, gsItem.count);
      warehouseGoodsDto.priceTotal = this.getPrice(gsItem.id, gsItem.count);
      goods.push(warehouseGoodsDto);
    });
    request.glWrites = glWrites;
    request.warehouseGoodsDtos = goods;
    console.log('REQUEST: ' + JSON.stringify(request));
    this.inputCashReceiptService.createInputCashReceipt(request).subscribe((resp) => {
      // this.router.navigate(['/menu']);
      this.savedId = resp.id;
    });
  }

  findAccountByAcctno(acctnoWithDesc: string, accounts: Array<GlAccount>): GlAccount {
    const strings = acctnoWithDesc.split('-');
    const acctno = strings[0].trim();
    const glAccounts = accounts.filter(account => account.acctNo === acctno);
    return glAccounts[0];
  }

  blurDesc($event: FocusEvent) {
    console.log('FocusEventType: ' + $event.type);
    console.log('FocusEvent: ' + this.accountingForm.get('description').value);
  }

  addRowGs() {
    const gsItem = new GsItem();
    gsItem.ident = this.identGsNext++;
    this.gsItems.push(gsItem);
  }

  calcTotalPrice(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getUnitPrice(item.id) * item.count;
    });
    return total;
  }

  calcTotalVat(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getVat(item.id, item.count);
    });
    return total;
  }

  calcTotalTotal(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getPrice(item.id, item.count);
    });
    return total;
  }

  delRowGs(rowsElement: GsItem) {
    this.gsItems = this.gsItems.filter(elem => elem.ident !== rowsElement.ident);
  }

  changeCompany(e) {
    this.crmCompany.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get crmCompany() {
    return this.accountingForm.get('crmCompany');
  }

  getUnitPrice(id: number): number {
    if (id) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return whGoodsLocal[0].packetInputPrice;
    }
    return 0;
  }

  getVat(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round((((whGoodsLocal[0].vatPercents / 100) *
        whGoodsLocal[0].packetInputPrice) * count) * 100) / 100;
    }
    return 0;
  }

  getPrice(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round((whGoodsLocal[0].packetInputPrice * count + this.getVat(id, count)) * 100) / 100;
    }
    return 0;
  }

  private readDataOutReceipt() {
    this.outputInvoiceService.getOutputGoodsReceiptNotInvoiced(this.page, this.size).subscribe((inputGoodsReceiptsResponse) => {
      this.inputGoodsReceipts = inputGoodsReceiptsResponse.goodsOutputReceipts.content;
      this.page = inputGoodsReceiptsResponse.goodsOutputReceipts.number;
      this.size = inputGoodsReceiptsResponse.goodsOutputReceipts.size;
      this.total = inputGoodsReceiptsResponse.goodsOutputReceipts.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.readDataOutReceipt();
  }

  generateAccounting() {
    this.accountService.getAccountsByGroup('CASH_AND_EQUIVALENTS').subscribe(tap => {
      console.log('cash: ' + tap[0].acctNo);
      const cash = tap[0].acctNo;
      this.accountService.getAccountsByGroup('REVENUE_GOODS').subscribe(eg => {
        console.log('gain from goods: ' + eg[0].acctNo);
        const gainFromGoods = eg[0].acctNo;
        const glAccDebit = this.getGlAccountByAcctno(cash);
        const glAccCredit = this.getGlAccountByAcctno(gainFromGoods);
        const amountWoVat = this.calcTotalPrice();
        const vatTotal = this.calcTotalVat();
        const amount = this.calcTotalTotal();
        this.rows = [];
        this.addRow();
        this.rows[0].debit.acctNoWithDesc = glAccDebit.acctNo + '-' + glAccDebit.description;
        this.rows[0].debit.amount = amount;
        this.rows[0].debit.analyticPart = String('000');
        this.rows[0].credit.acctNoWithDesc = glAccCredit.acctNo + '-' + glAccCredit.description;
        this.rows[0].credit.amount = amountWoVat;
        this.rows[0].credit.analyticPart = String('000');


        this.accountService.getAccountsByGroup('VAT').subscribe(vat => {
          console.log('vat account: ' + vat[0].acctNo);
          const accountVatAcctno = vat[0].acctNo;
          const glAccVat = this.getGlAccountByAcctno(accountVatAcctno);

          this.addRow();
          this.rows[1].credit.acctNoWithDesc = glAccVat.acctNo + '-' + glAccVat.description;
          this.rows[1].credit.amount = vatTotal;
          this.rows[1].credit.analyticPart = String('000');
        });
      });
    });
  }

  getGlAccountByAcctno(acctno: string): GlAccount {
    const account = this.accountsDebit.filter(acc => acc.acctNo === acctno);
    return account[0];
  }

  changeCashierTitle(e) {
    this.cashierTitle.setValue(e.target.value, {
      onlySelf: true
    });
  }

  toggle(outputGoodsReceipt: OutputGoodsReceipt) {
    this.griIds.push(outputGoodsReceipt.id);
    outputGoodsReceipt.selected = !outputGoodsReceipt.selected;
    if (outputGoodsReceipt.selected) {
      outputGoodsReceipt.goodsServiceItems.forEach(itemOrig => {
        const gsItem = new GsItem();
        gsItem.id = itemOrig.warehouseGoodsServices.goodsServices.id;
        gsItem.name = itemOrig.warehouseGoodsServices.goodsServices.name;
        gsItem.count = itemOrig.numberOfUnits;
        gsItem.ident = this.identGsNext++;
        this.gsItems.push(gsItem);
      });
    } else {
      this.gsItems = [];
      this.griIds = [];
    }
  }

  get cashierTitle() {
    return this.accountingForm.get('cashierTitle');
  }

  generatePdf(id: number) {
    this.inputCashReceiptService.getPdf(id).subscribe(responseMessage => {
      const file = new Blob([responseMessage], { type: 'application/pdf' });
      const fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    });
  }
}
