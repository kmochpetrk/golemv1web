import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputCashReceiptComponent } from './input-cash-receipt.component';

describe('InputCashReceiptComponent', () => {
  let component: InputCashReceiptComponent;
  let fixture: ComponentFixture<InputCashReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputCashReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputCashReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
