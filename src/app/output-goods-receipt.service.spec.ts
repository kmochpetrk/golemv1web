import { TestBed } from '@angular/core/testing';

import { OutputGoodsReceiptService } from './output-goods-receipt.service';

describe('OutputGoodsReceiptService', () => {
  let service: OutputGoodsReceiptService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OutputGoodsReceiptService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
