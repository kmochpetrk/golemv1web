import {Injectable} from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {BankAccountResponse, CompaniesResponse, EshopProductPageResponse, OrderFull, OrderFullForAcc} from '../models/eshop';
import {NextNumberResponse} from '../models/NextNumberResponse';

@Injectable()
export class EshopService {

  selectedOrderFulls: OrderFull[] = [];

  constructor(private authService: AuthenticationService, private http: HttpClient) {
  }

  public getEshops(company: string, page: number, size: number): Observable<EshopProductPageResponse> {
    return this.http.get<EshopProductPageResponse>(environment.baseUrl +
      '/eshop/' + company + '?page=' + page + '&size=' + size, this.authService.createOptionsWithoutAuth());
  }

  nextInvoiceNumber(company: string): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/eshop/nextnumber/' + company, this.authService.createOptionsWithoutAuth());
  }

  getAccountNumber(company: string): Observable<BankAccountResponse> {
    return this.http.get<BankAccountResponse>(environment.baseUrl +
      '/eshop/bankaccount/' + company, this.authService.createOptionsWithoutAuth());
  }

  createOrder(orderFullForAcc: OrderFullForAcc, company: string): Observable<OrderFullForAcc> {
    return this.http.post<OrderFullForAcc>(environment.baseUrl + '/eshop/order/' + company, orderFullForAcc,
      this.authService.createOptionsWithoutAuth());
  }

  getCompanies(): Observable<CompaniesResponse> {
    return this.http.get<CompaniesResponse>(environment.baseUrl +
      '/eshop/companies', this.authService.createOptionsWithoutAuth());
  }
}
