import {Component, OnDestroy, OnInit} from '@angular/core';
import {EshopService} from '../eshop.service';
import {OrderFull, OrderFullForAcc} from '../../models/eshop';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Address, Crm} from '../../models/crms';

@Component({
  selector: 'app-eshop-checkout',
  templateUrl: './eshop-checkout.component.html',
  styleUrls: ['./eshop-checkout.component.css']
})
export class EshopCheckoutComponent implements OnInit, OnDestroy {
  orders: OrderFull[];
  ordersForm: FormGroup;
  private sub: Subscription;
  paymentTypes = ['delivery_cash', 'bank_transfer', 'paypal', 'platba24'];
  deliveryTypes = ['front door', 'after front door', 'second door', 'at warehouse'];
  company: string;
  bankAccount: string;
  oINumber: string;

  constructor(private formBuilder: FormBuilder, private eshopService: EshopService,
              private activatedRoute: ActivatedRoute, private router: Router) {
    this.ordersForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      ico: [''],
      dic: [''],
      paymentType: ['', [Validators.required]],
      deliveryType: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      street: ['', [Validators.required]],
      descNumber: ['', [Validators.required]],
      city: ['', [Validators.required]],
      zipCode: ['', [Validators.required]],
      documentNumber: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.orders = this.eshopService.selectedOrderFulls;
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.company = params.company;
      this.eshopService.nextInvoiceNumber(this.company).subscribe(resp => {
        this.ordersForm.get('documentNumber').setValue(resp.documentNumber, {
          onlySelf: true, emitEvent: false
        });
        this.oINumber = resp.documentNumber;
      });
      this.eshopService.getAccountNumber(this.company).subscribe(resp => {
        this.bankAccount = resp.bankAccount;
      });
    });
  }

  confirm() {
    if (this.ordersForm.valid) {
      this.orders.forEach(order => {
        order.item.totalItemVat = this.calcRowVat(order);
        order.item.totalItemPrice = this.calcRowPriceWithVat(order);
      });
      const orderFullForAcc = new OrderFullForAcc();
      orderFullForAcc.orders = this.orders;
      orderFullForAcc.paymentType = this.ordersForm.get('paymentType').value.split(':')[1].trim();
      orderFullForAcc.deliveryType = this.ordersForm.get('deliveryType').value.split(':')[1].trim();
      orderFullForAcc.totalVat = this.calcTotalVat();
      orderFullForAcc.totalPrice = this.calcTotalTotal();
      orderFullForAcc.oINumber = this.oINumber;
      orderFullForAcc.ourBankAccount = this.bankAccount;
      orderFullForAcc.company = this.company;
      const crm = new Crm();
      crm.name = this.ordersForm.get('name').value;
      crm.dic = this.ordersForm.get('dic').value;
      crm.ico = this.ordersForm.get('ico').value;
      crm.email = this.ordersForm.get('email').value;
      crm.phone = this.ordersForm.get('phone').value;
      crm.primaryAddress = new Address();
      crm.primaryAddress.street = this.ordersForm.get('street').value;
      crm.primaryAddress.descNumber = this.ordersForm.get('descNumber').value;
      crm.primaryAddress.city = this.ordersForm.get('city').value;
      crm.primaryAddress.zipCode = this.ordersForm.get('zipCode').value;
      orderFullForAcc.crm = crm;
      console.log('Order: ' + JSON.stringify(orderFullForAcc));
      this.eshopService.createOrder(orderFullForAcc, this.company).subscribe(resp => {
        console.log('order saved!');
        this.router.navigate(['/']);
      });
    }
  }

  changePaymentType(e) {
    this.paymentType.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get paymentType() {
    return this.ordersForm.get('paymentType');
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  calcRowVat(order: OrderFull) {
    return Math.round(order.item.vatPercent * order.item.orderedCount * order.item.unitOutputPrice) / 100;
  }

  calcRowPriceWithVat(order: OrderFull) {
    return Math.round((100 + order.item.vatPercent) * order.item.orderedCount * order.item.unitOutputPrice) / 100;
  }

  calcTotalVat() {
    let totalVat = 0;
    this.orders.forEach(order => {
      totalVat += this.calcRowVat(order);
    });
    return totalVat;
  }

  calcTotalTotal() {
    let totalPrice = 0;
    this.orders.forEach(order => {
      totalPrice += this.calcRowPriceWithVat(order);
    });
    return totalPrice;
  }

  changeDeliveryType(e) {
    this.deliveryType.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get deliveryType() {
    return this.ordersForm.get('deliveryType');
  }
}

