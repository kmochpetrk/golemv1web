import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EshopCheckoutComponent } from './eshop-checkout.component';

describe('EshopCheckoutComponent', () => {
  let component: EshopCheckoutComponent;
  let fixture: ComponentFixture<EshopCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EshopCheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EshopCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
