import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EshopProductComponent } from './eshop-product.component';

describe('EshopProductComponent', () => {
  let component: EshopProductComponent;
  let fixture: ComponentFixture<EshopProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EshopProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EshopProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
