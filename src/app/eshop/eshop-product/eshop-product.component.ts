import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {EshopGoodsService, Item, Order, OrderFull} from '../../models/eshop';
import {EshopService} from '../eshop.service';

@Component({
  selector: 'app-eshop-product',
  templateUrl: './eshop-product.component.html',
  styleUrls: ['./eshop-product.component.css']
})
export class EshopProductComponent implements OnInit, OnDestroy {
  eshopGoodsServices: EshopGoodsService[];
  selectedOrderFulls: OrderFull[] = [];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];
  private sub: Subscription;
  company: string;
  selectedGoodsService: EshopGoodsService;
  orders: Order[] = [];

  constructor(private eshopService: EshopService, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.company = params.company;
      this.readData(this.page, this.size);
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.eshopService.getEshops(this.company, this.page, this.size).subscribe(eshopsResponse => {
      this.eshopGoodsServices = eshopsResponse.goodsServices.content;
      this.page = eshopsResponse.goodsServices.number;
      this.size = eshopsResponse.goodsServices.size;
      this.total = eshopsResponse.goodsServices.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  addWhGoods() {
    this.router.navigate(['/edit-wh-goods']);
  }

  ngOnDestroy(): void {
  }

  select(eshopGoodsService: EshopGoodsService) {
    this.selectedGoodsService = eshopGoodsService;
  }

  order(selectedGoodsService: Item) {
    console.log('Order: ' + JSON.stringify(selectedGoodsService));
    if (this.isOrderedItem(selectedGoodsService)) {
      this.orders = this.orders.filter(order => (order.goodsServiceId !== this.selectedGoodsService.goodsServiceId ||
        selectedGoodsService.warehouseId !== order.item.warehouseId));
      this.selectedOrderFulls = this.selectedOrderFulls.filter(orderFull =>
        this.isOrderedItem(orderFull.item, orderFull.eshopGoodsService.goodsServiceId));
    } else {
      this.orders.push({
        count: selectedGoodsService.orderedCount, goodsServiceId: this.selectedGoodsService.goodsServiceId,
        item: selectedGoodsService
      });
      this.selectedOrderFulls.push({eshopGoodsService: this.selectedGoodsService, item: selectedGoodsService});
    }
  }

  calcTotalFromSelected(selectedItem: Item) {
    if (selectedItem.orderedCount) {
      return Math.round(selectedItem.orderedCount * selectedItem.unitOutputPriceWithVat * 100) / 100;
    }
    return null;
  }

  isDisabled(eshopGoodsServiceItem: Item) {
    return !eshopGoodsServiceItem.orderedCount || eshopGoodsServiceItem.orderedCount <= 0;
  }

  isOrderedItem(item: Item, gsId= this.selectedGoodsService.goodsServiceId): boolean {
    // const gsId = this.selectedGoodsService.goodsServiceId;
    let result = false;
    this.orders.forEach(order => {
      if (order.goodsServiceId === gsId && order.item.warehouseId === item.warehouseId) {
        result = true;
      }
    });
    return result;
  }

  routeToCheckout() {
    this.eshopService.selectedOrderFulls = this.selectedOrderFulls;
    this.router.navigate(['/eshop/' + this.company + '/eshop-checkout']);
  }
}
