import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {GoodsOutputReceiptRequest} from './models/InputGoodsReceipt';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class OutputGoodsReceiptService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createOutputGoodsReceipt(goodsOutputReceiptRequest: GoodsOutputReceiptRequest): Observable<any> {
    return this.http.post<any>(environment.baseUrl +
      '/goodsoutputreceipt', goodsOutputReceiptRequest,  this.authService.createOptions());
  }

  public nextDoumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/goodsoutputreceipt/nextnumber', this.authService.createOptions());
  }
}
