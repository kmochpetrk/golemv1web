import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Employee} from '../models/crms';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit, OnDestroy {

  editEmployeeForm: FormGroup;

//  readCrm: Crm;



  private sub: Subscription;
  id: number;

  constructor(private formBuilder: FormBuilder, public activatedRoute: ActivatedRoute,
              private employeeService: EmployeeService, private router: Router) {
    this.editEmployeeForm = this.formBuilder.group({
      surname: ['', [Validators.required]],
      firstname: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', []],
      employeeNumber: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      birthNumber: ['', [Validators.required]],
      monthlyGrossSalary: ['', [Validators.required]],
      personalBankAccount: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.employeeService.nextDocumentNumber().subscribe(resp => {
      this.editEmployeeForm.get('employeeNumber').setValue(resp.documentNumber, {
        onlySelf: true, emitEvent: false
      });
    });
    // this.sub = this.activatedRoute.params.subscribe(params => {
    //   this.id = +params.id;
    //   if (this.id) {
    //     this.crmsService.getCrmById(this.id).subscribe((crm) => {
    //       this.readCrm = crm;
    //       this.editCrmForm.get('name').setValue(crm.name, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('ico').setValue(crm.ico, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('dic').setValue(crm.dic, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('email').setValue(crm.email, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('phone').setValue(crm.phone, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('bankAccount').setValue(crm.bankAccount, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       if (crm.primaryAddress) {
    //         this.editCrmForm.get('street').setValue(crm.primaryAddress.street, {
    //           onlySelf: true, emitEvent: false
    //         });
    //         this.editCrmForm.get('descNumber').setValue(crm.primaryAddress.descNumber, {
    //           onlySelf: true, emitEvent: false
    //         });
    //         this.editCrmForm.get('city').setValue(crm.primaryAddress.city, {
    //           onlySelf: true, emitEvent: false
    //         });
    //         this.editCrmForm.get('zipCode').setValue(crm.primaryAddress.zipCode, {
    //           onlySelf: true, emitEvent: false
    //         });
    //       }
    //       const crmType = this.crmTypes.indexOf(crm.crmType) + ': ' + crm.crmType;
    //       this.editCrmForm.get('crmType').setValue(crmType, {
    //         onlySelf: true, emitEvent: false
    //       });
    //     });
    //   }
    // });
  }

  submitEmployeeForm() {
    this.editEmployeeForm.updateValueAndValidity();
    if (this.editEmployeeForm.valid) {
      const employee = new Employee();
      if (this.id) {
        employee.id = this.id;
      }
      employee.firstname = this.editEmployeeForm.get('firstname').value;
      employee.surname = this.editEmployeeForm.get('surname').value;
      employee.startDate = this.editEmployeeForm.get('startDate').value;
      employee.endDate = this.editEmployeeForm.get('endDate').value;
      employee.monthlyGrossSalary = this.editEmployeeForm.get('monthlyGrossSalary').value;
      employee.birthDate = this.editEmployeeForm.get('birthDate').value;
      employee.birthNumber = this.editEmployeeForm.get('birthNumber').value;
      employee.employeeNumber = this.editEmployeeForm.get('employeeNumber').value;
      employee.personalBankAccount = this.editEmployeeForm.get('personalBankAccount').value;
      if (!this.id) {
        console.log(JSON.stringify('inventoryItem: ' + employee));
        this.employeeService.createEmployee(employee).subscribe((resp) => {
          console.log('Employee saved.');
          this.router.navigate(['/menu']);
        });
      }
      // else {
      //   this.inventoryService.updateCrm(crm).subscribe((resp) => {
      //     console.log('Crm saved.');
      //     this.router.navigate(['/crms']);
      //   });
      // }
    }
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
