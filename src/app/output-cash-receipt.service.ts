import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {OutputCashReceiptRequest, OutputCashReceiptResponse} from './models/invoices';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class OutputCashReceiptService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createOutputCashReceipt(outputCashReceiptRequest: OutputCashReceiptRequest): Observable<OutputCashReceiptResponse> {
    return this.http.post<OutputCashReceiptResponse>(environment.baseUrl +
      '/output-cash-receipt', outputCashReceiptRequest,  this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/output-cash-receipt/nextnumber', this.authService.createOptions());
  }

  public getPdf(id: number): Observable<any> {
    const options = this.authService.createOptionsPdf();
    return this.http.get<any>(environment.baseUrl +
      '/output-cash-receipt/pdf/' + id, options);
  }
}
