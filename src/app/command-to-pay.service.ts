import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CommandToPay, CommandToPaysResponse} from './models/accounts';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class CommandToPayService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getCommandToPays(page: number, size: number): Observable<CommandToPaysResponse> {
    return this.http.get<CommandToPaysResponse>(environment.baseUrl +
      '/command-to-pay?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public getCommandToPaysNotCtp(page: number, size: number): Observable<CommandToPaysResponse> {
    return this.http.get<CommandToPaysResponse>(environment.baseUrl +
      '/command-to-pay/ctp-null?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public createCommandToPay(commandToPay: CommandToPay): Observable<CommandToPay> {
    return this.http.post<CommandToPay>(environment.baseUrl + '/command-to-pay', commandToPay,
      this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/command-to-pay/nextnumber', this.authService.createOptions());
  }
}
