import {Component, OnInit} from '@angular/core';
import {InternalOrder} from '../models/accounts';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';
import {InternalOrderService} from '../internal-order.service';

@Component({
  selector: 'app-internal-order',
  templateUrl: './internal-order.component.html',
  styleUrls: ['./internal-order.component.css']
})
export class InternalOrderComponent implements OnInit {
  internalOrders: InternalOrder[];
  selectedInternalOrder: InternalOrder;
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  constructor(public authenticationService: AuthenticationService,
              private internalOrderService: InternalOrderService, private router: Router) { }

  ngOnInit(): void {
    this.internalOrderService.getInternalOrders(this.page, this.size).subscribe(internalOrdersResponse => {
      this.internalOrders = internalOrdersResponse.internalOrders.content;
      this.page = internalOrdersResponse.internalOrders.number;
      this.size = internalOrdersResponse.internalOrders.size;
      this.total = internalOrdersResponse.internalOrders.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  selectBooking(internalOrder: InternalOrder) {
    this.selectedInternalOrder = internalOrder;
  }

  processOrder(internalOrder: InternalOrder) {
    // todo OGR and OI
  }
}
