import { TestBed } from '@angular/core/testing';

import { OutputInvoiceService } from './output-invoice.service';

describe('OutputInvoiceService', () => {
  let service: OutputInvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OutputInvoiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
