import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {Crm, CrmsResponse} from './models/crms';

@Injectable()
export class CrmsService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getCrms(page: number, size: number): Observable<CrmsResponse> {
    return this.http.get<CrmsResponse>(environment.baseUrl +
      '/crm?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public createCrm(crm: Crm): Observable<Crm> {
    return this.http.post<Crm>(environment.baseUrl +
      '/crm', crm, this.authService.createOptions());
  }

  public updateCrm(crm: Crm): Observable<Crm> {
    return this.http.put<Crm>(environment.baseUrl +
      '/crm/' + crm.id, crm, this.authService.createOptions());
  }

  public getCrmById(id: number): Observable<Crm> {
    return this.http.get<Crm>(environment.baseUrl +
      '/crm/' + id, this.authService.createOptions());
  }

  public deleteCrmById(id: number): Observable<Crm> {
    return this.http.delete<Crm>(environment.baseUrl +
      '/crm/' + id, this.authService.createOptions());
  }

}
