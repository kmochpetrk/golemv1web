import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {InventoryItem} from '../models/crms';
import {ActivatedRoute, Router} from '@angular/router';
import {InventoryService} from '../inventory.service';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit, OnDestroy {
  depreciationTypes = ['REGULAR', 'ACCELERATED'];
  depreciationClasses = ['A', 'B', 'C', 'D'];
  propertyKinds = ['TANGIBLE', 'INTANGIBLE'];
  editInventoryForm: FormGroup;

//  readCrm: Crm;



  private sub: Subscription;
  id: number;

  constructor(private formBuilder: FormBuilder, public activatedRoute: ActivatedRoute,
              private inventoryService: InventoryService, private router: Router) {
    this.editInventoryForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', []],
      inventoryNumber: ['', [Validators.required]],
      depreciationType: ['', [Validators.required]],
      depreciationClass: ['', [Validators.required]],
      propertyKind: ['', [Validators.required]],
      price: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.inventoryService.nextDocumentNumber().subscribe(resp => {
      this.editInventoryForm.get('inventoryNumber').setValue(resp.documentNumber, {
        onlySelf: true, emitEvent: false
      });
    });
    // this.sub = this.activatedRoute.params.subscribe(params => {
    //   this.id = +params.id;
    //   if (this.id) {
    //     this.crmsService.getCrmById(this.id).subscribe((crm) => {
    //       this.readCrm = crm;
    //       this.editCrmForm.get('name').setValue(crm.name, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('ico').setValue(crm.ico, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('dic').setValue(crm.dic, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('email').setValue(crm.email, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('phone').setValue(crm.phone, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       this.editCrmForm.get('bankAccount').setValue(crm.bankAccount, {
    //         onlySelf: true, emitEvent: false
    //       });
    //       if (crm.primaryAddress) {
    //         this.editCrmForm.get('street').setValue(crm.primaryAddress.street, {
    //           onlySelf: true, emitEvent: false
    //         });
    //         this.editCrmForm.get('descNumber').setValue(crm.primaryAddress.descNumber, {
    //           onlySelf: true, emitEvent: false
    //         });
    //         this.editCrmForm.get('city').setValue(crm.primaryAddress.city, {
    //           onlySelf: true, emitEvent: false
    //         });
    //         this.editCrmForm.get('zipCode').setValue(crm.primaryAddress.zipCode, {
    //           onlySelf: true, emitEvent: false
    //         });
    //       }
    //       const crmType = this.crmTypes.indexOf(crm.crmType) + ': ' + crm.crmType;
    //       this.editCrmForm.get('crmType').setValue(crmType, {
    //         onlySelf: true, emitEvent: false
    //       });
    //     });
    //   }
    // });
  }

  submitInventoryForm() {
    this.editInventoryForm.updateValueAndValidity();
    if (this.editInventoryForm.valid) {
      const inventoryItem = new InventoryItem();
      if (this.id) {
        inventoryItem.id = this.id;
      }
      inventoryItem.name = this.editInventoryForm.get('name').value;
      inventoryItem.inventoryNumber = this.editInventoryForm.get('inventoryNumber').value;
      inventoryItem.startDate = this.editInventoryForm.get('startDate').value;
      inventoryItem.endDate = this.editInventoryForm.get('endDate').value;
      inventoryItem.price = this.editInventoryForm.get('price').value;
      inventoryItem.depreciationType = this.editInventoryForm.get('depreciationType').value.split(':')[1].trim();
      inventoryItem.depreciationClass = this.editInventoryForm.get('depreciationClass').value.split(':')[1].trim();
      inventoryItem.propertyKind = this.editInventoryForm.get('propertyKind').value.split(':')[1].trim();
      if (!this.id) {
        console.log(JSON.stringify('inventoryItem: ' + inventoryItem));
        this.inventoryService.createInventoryItem(inventoryItem).subscribe((resp) => {
          console.log('Inventory saved.');
          this.router.navigate(['/menu']);
        });
      }
      // else {
      //   this.inventoryService.updateCrm(crm).subscribe((resp) => {
      //     console.log('Crm saved.');
      //     this.router.navigate(['/crms']);
      //   });
      // }
    }
  }

  changeDepreciationType(e) {
    this.depreciationType.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get depreciationType() {
    return this.editInventoryForm.get('depreciationType');
  }

  changeDepreciationClass(e) {
    this.depreciationClass.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get depreciationClass() {
    return this.editInventoryForm.get('depreciationClass');
  }

  changePropertyKind(e) {
    this.propertyKind.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get propertyKind() {
    return this.editInventoryForm.get('propertyKind');
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
