import { Injectable } from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {WarehousesResponse} from './models/warehouses';
import {environment} from '../environments/environment';
import {WhGoodsResponse} from './models/wh-goods';

@Injectable()
export class WgGoodsService {

  constructor(private authService: AuthenticationService, private http: HttpClient) {
  }

  public getWhGoods(warehouseId: number, page: number, size: number): Observable<WhGoodsResponse> {
    return this.http.get<WhGoodsResponse>(environment.baseUrl +
      '/warehousegoodsservices/' + warehouseId + '?page=' + page + '&size=' + size, this.authService.createOptions());
  }
}
