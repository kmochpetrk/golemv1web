import { Component, OnInit } from '@angular/core';
import {GlAccount} from '../models/accounts';
import {AuthenticationService} from '../authentication.service';
import {AccountService} from '../account.service';
import {Router} from '@angular/router';
import {GoodsServicesService} from '../goods-services.service';
import {GoodsServices} from '../models/goods-services';

@Component({
  selector: 'app-goods-services',
  templateUrl: './goods-services.component.html',
  styleUrls: ['./goods-services.component.css']
})
export class GoodsServicesComponent implements OnInit {

  goodsServices: GoodsServices[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  constructor(public authenticationService: AuthenticationService,
              private goodsServicesService: GoodsServicesService, private router: Router) { }

  ngOnInit(): void {
    this.goodsServicesService.getGoodsServices(this.page, this.size).subscribe(goodsServicesResponse => {
      this.goodsServices = goodsServicesResponse.goodsServices.content;
      this.page = goodsServicesResponse.goodsServices.number;
      this.size = goodsServicesResponse.goodsServices.size;
      this.total = goodsServicesResponse.goodsServices.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  editGoodsServices(goodsServices: GoodsServices) {
    this.router.navigate(['/edit-goods-services', goodsServices.id]);
    // this.accountsService.updateAccount(account).subscribe((resp) => {
    //   console.log('edit account');
    //   this.ngOnInit();
    // });
  }

  createGoodsServices() {
    this.router.navigate(['/edit-goods-services']);
  }

  deleteGoodsServices(goodsServices: GoodsServices) {
    this.goodsServicesService.deleteGoodsServicesById(goodsServices.id).subscribe((resp) => {
      console.log('delete account');
      this.page = 0;
      this.ngOnInit();
    });
  }

}
