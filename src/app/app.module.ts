import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import {RouterModule} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccountService} from './account.service';
import { GlAccountsComponent } from './gl-accounts/gl-accounts.component';
import { MenuComponent } from './menu/menu.component';
import { EditGlaccountComponent } from './edit-glaccount/edit-glaccount.component';
import { GoodsServicesComponent } from './goods-services/goods-services.component';
import {GoodsServicesService} from './goods-services.service';
import { EditGoodsServicesComponent } from './edit-goods-services/edit-goods-services.component';
import { GlBookingComponent } from './gl-booking/gl-booking.component';
import { GeneralAccountingComponent } from './general-accounting/general-accounting.component';
import { CrmsComponent } from './crms/crms.component';
import {CrmsService} from './crms.service';
import { EditCrmComponent } from './edit-crm/edit-crm.component';
import { WarehousesComponent } from './warehouses/warehouses.component';
import {WarehouseService} from './warehouse.service';
import { WhGoodsComponent } from './wh-goods/wh-goods.component';
import {WgGoodsService} from './wg-goods.service';
import { InputGoodsReceiptComponent } from './input-goods-receipt/input-goods-receipt.component';
import {InputGoodsReceiptService} from './input-goods-receipt.service';
import { OutputGoodsReceiptComponent } from './output-goods-receipt/output-goods-receipt.component';
import {OutputGoodsReceiptService} from './output-goods-receipt.service';
import { OutputInvoiceComponent } from './output-invoice/output-invoice.component';
import {OutputInvoiceService} from './output-invoice.service';
import { InputInvoiceComponent } from './input-invoice/input-invoice.component';
import {InputInvoiceService} from './input-invoice.service';
import { EshopProductComponent } from './eshop/eshop-product/eshop-product.component';
import {EshopService} from './eshop/eshop.service';
import { EshopCheckoutComponent } from './eshop/eshop-checkout/eshop-checkout.component';
import { BookingsComponent } from './bookings/bookings.component';
import {BookingsService} from './bookings.service';
import { BankStatementIncomeComponent } from './bank-statement-income/bank-statement-income.component';
import {BankStatementService} from './bank-statement.service';
import { BankStatementOutcomeComponent } from './bank-statement-outcome/bank-statement-outcome.component';
import { InputCashReceiptComponent } from './input-cash-receipt/input-cash-receipt.component';
import {InputCashReceiptService} from './input-cash-receipt.service';
import { OutputCashReceiptComponent } from './output-cash-receipt/output-cash-receipt.component';
import {OutputCashReceiptService} from './output-cash-receipt.service';
import { BalanceSheetComponent } from './balance-sheet/balance-sheet.component';
import {AccountingStatementsService} from './accounting-statements.service';
import { ProfitLossComponent } from './profit-loss/profit-loss.component';
import { InternalOrderComponent } from './internal-order/internal-order.component';
import {InternalOrderService} from './internal-order.service';
import { CommandToPayComponent } from './command-to-pay/command-to-pay.component';
import {CommandToPayService} from './command-to-pay.service';
import { CommandToPayListComponent } from './command-to-pay-list/command-to-pay-list.component';
import { CreateCompanyComponent } from './create-company/create-company.component';
import {CreateCompanyService} from './create-company.service';
import { CreateWarehouseComponent } from './create-warehouse/create-warehouse.component';
import { InventoryComponent } from './inventory/inventory.component';
import {InventoryService} from './inventory.service';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { EmployeeCreateComponent } from './employee-create/employee-create.component';
import {EmployeeService} from './employee.service';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { InputInvoiceNotPaidComponent } from './input-invoice-not-paid/input-invoice-not-paid.component';
import { OutputInvoiceNotPaidComponent } from './output-invoice-not-paid/output-invoice-not-paid.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    GlAccountsComponent,
    MenuComponent,
    EditGlaccountComponent,
    GoodsServicesComponent,
    EditGoodsServicesComponent,
    GlBookingComponent,
    GeneralAccountingComponent,
    CrmsComponent,
    EditCrmComponent,
    WarehousesComponent,
    WhGoodsComponent,
    InputGoodsReceiptComponent,
    OutputInvoiceComponent,
    OutputGoodsReceiptComponent,
    InputInvoiceComponent,
    EshopProductComponent,
    EshopCheckoutComponent,
    BookingsComponent,
    BankStatementIncomeComponent,
    BankStatementOutcomeComponent,
    InputCashReceiptComponent,
    OutputCashReceiptComponent,
    BalanceSheetComponent,
    ProfitLossComponent,
    InternalOrderComponent,
    CommandToPayComponent,
    CommandToPayListComponent,
    CreateCompanyComponent,
    CreateWarehouseComponent,
    InventoryComponent,
    InventoryListComponent,
    EmployeeCreateComponent,
    EmployeeListComponent,
    InputInvoiceNotPaidComponent,
    OutputInvoiceNotPaidComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [AuthenticationService, AccountService, GoodsServicesService, InputInvoiceService, EshopService, BookingsService,
    CrmsService, WarehouseService, WgGoodsService, InputGoodsReceiptService, OutputGoodsReceiptService, OutputInvoiceService,
    BankStatementService, InputCashReceiptService, OutputCashReceiptService, AccountingStatementsService, InternalOrderService,
    CommandToPayService, CreateCompanyService, InventoryService, EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
