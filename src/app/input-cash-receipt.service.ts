import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {InputCashReceiptRequest, InputCashReceiptResponse} from './models/invoices';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class InputCashReceiptService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createInputCashReceipt(inputCashReceiptRequest: InputCashReceiptRequest): Observable<InputCashReceiptResponse> {
    return this.http.post<InputCashReceiptResponse>(environment.baseUrl +
      '/input-cash-receipt', inputCashReceiptRequest,  this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/input-cash-receipt/nextnumber', this.authService.createOptions());
  }

  public getPdf(id: number): Observable<any> {
    const options = this.authService.createOptionsPdf();
    return this.http.get<any>(environment.baseUrl +
      '/input-cash-receipt/pdf/' + id, options);
  }
}
