import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {GoodsInputReceiptRequest} from './models/InputGoodsReceipt';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class InputGoodsReceiptService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createINputGoodsReceipt(goodsInputReceiptRequest: GoodsInputReceiptRequest): Observable<any> {
    return this.http.post<any>(environment.baseUrl +
      '/goodsinputreceipt', goodsInputReceiptRequest,  this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/goodsinputreceipt/nextnumber', this.authService.createOptions());
  }
}
