import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {Warehouse, WarehousesResponse} from './models/warehouses';

@Injectable()
export class WarehouseService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getWarehouses(page: number, size: number): Observable<WarehousesResponse> {
    return this.http.get<WarehousesResponse>(environment.baseUrl +
      '/warehouse?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public createWarehouse(warehouse: Warehouse): Observable<Warehouse> {
    return this.http.post<Warehouse>(environment.baseUrl +
      '/warehouse', warehouse, this.authService.createOptions());
  }

  public updateWarehouse(warehouse: Warehouse): Observable<Warehouse> {
    return this.http.put<Warehouse>(environment.baseUrl +
      '/warehouse/' + warehouse.id, warehouse, this.authService.createOptions());
  }

  public getWarehouseById(id: number): Observable<Warehouse> {
    return this.http.get<Warehouse>(environment.baseUrl +
      '/warehouse/' + id, this.authService.createOptions());
  }

  public deleteWarehouseById(id: number): Observable<Warehouse> {
    return this.http.delete<Warehouse>(environment.baseUrl +
      '/warehouse/' + id, this.authService.createOptions());
  }

}
