import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {GoodsOutputReceiptsResponse, OutputInvoiceRequest, OutputInvoiceResponse, OutputInvoicesSmallResponse} from './models/invoices';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class OutputInvoiceService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createOutputInvoice(outputInvoiceRequest: OutputInvoiceRequest): Observable<OutputInvoiceResponse> {
    return this.http.post<any>(environment.baseUrl +
      '/outputinvoice', outputInvoiceRequest,  this.authService.createOptions());
  }

  public getPdf(id: number): Observable<any> {
    const options = this.authService.createOptionsPdf();
    return this.http.get<any>(environment.baseUrl +
      '/outputinvoice/pdf/' + id, options);
  }

  public getOutputGoodsReceiptNotInvoiced(page: number, size: number): Observable<GoodsOutputReceiptsResponse> {
    return this.http.get<GoodsOutputReceiptsResponse>(environment.baseUrl +
      '/goodsoutputreceipt/notinvoiced?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public getOutputInvoicesSmall(page: number, size: number): Observable<OutputInvoicesSmallResponse> {
    return this.http.get<OutputInvoicesSmallResponse>(environment.baseUrl +
      '/outputinvoice/small?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/outputinvoice/nextnumber', this.authService.createOptions());
  }
}
