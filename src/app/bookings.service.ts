import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Booking, BookingsResponse, InputInvoicesNotPaidResponse, OutputInvoicesNotPaidResponse} from './models/accounts';
import {environment} from '../environments/environment';
import {GeneralAccountingPostRequest} from './models/general-accounting-train-request';

@Injectable()
export class BookingsService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getBookings(page: number, size: number, docType: string): Observable<BookingsResponse> {
    return this.http.get<BookingsResponse>(environment.baseUrl +
      '/general_accounting/bookings?page=' + page + '&size=' + size + '&docType=' + docType, this.authService.createOptions());
  }

  public createBooking(generalAccountingPostRequest: GeneralAccountingPostRequest): Observable<Booking> {
    return this.http.post<Booking>(environment.baseUrl + '/general_accounting/bookings', generalAccountingPostRequest,
      this.authService.createOptions());
  }

  public cancel(booking: Booking): Observable<Booking> {
    return this.http.put<Booking>(environment.baseUrl + '/general_accounting/bookings', booking,
      this.authService.createOptions());
  }

  public getInputInvoicesNotPaid(page: number, size: number): Observable<InputInvoicesNotPaidResponse> {
    return this.http.get<InputInvoicesNotPaidResponse>(environment.baseUrl +
      '/inputinvoice/not-paid?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public getOutputInvoicesNotPaid(page: number, size: number): Observable<OutputInvoicesNotPaidResponse> {
    return this.http.get<OutputInvoicesNotPaidResponse>(environment.baseUrl +
      '/outputinvoice/not-paid?page=' + page + '&size=' + size, this.authService.createOptions());
  }
}
