import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';
import {ComplexDepreciation, DepreciationItem, InventoryItem} from '../models/crms';
import {InventoryService} from '../inventory.service';
import {GlWriteFe, GlWriteFeBoth} from '../models/gl-writes';
import {GlAccount} from '../models/accounts';
import {AccountService} from '../account.service';
import {combineAll} from 'rxjs/operators';
import {GlWriteDto} from '../models/InputGoodsReceipt';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.css']
})
export class InventoryListComponent implements OnInit {
  inventoryItems: InventoryItem[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];

  rows: Array<GlWriteFeBoth> = [];
  selectedRow: GlWriteFeBoth;
  identNext = 0;
  selectedInventoryItem: InventoryItem;
  newDepreciationItems = [];

  constructor(public authenticationService: AuthenticationService, private accountService: AccountService,
              private inventoryService: InventoryService, private router: Router) { }

  ngOnInit(): void {
    this.inventoryService.getInventoryItems(this.page, this.size).subscribe(accountsResponse => {
      this.inventoryItems = accountsResponse.inventoryItems.content;
      this.page = accountsResponse.inventoryItems.number;
      this.size = accountsResponse.inventoryItems.size;
      this.total = accountsResponse.inventoryItems.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.accountsDebit = accounts.accounts.content;
      this.accountsCredit = accounts.accounts.content;
    });
  }

  generateAccounting(amount: number) {
    this.accountService.getAccountsByGroup('AMORTIZATION').subscribe(tap => {
      console.log('amortization: ' + tap[0].acctNo);
      const amortization = tap[0].acctNo;
      this.accountService.getAccountsByGroup('DEPRECIATION').subscribe(eg => {
        console.log('depreciation: ' + eg[0].acctNo);
        const gainFromGoods = eg[0].acctNo;
        const glAccDebit = this.getGlAccountByAcctno(amortization);
        const glAccCredit = this.getGlAccountByAcctno(gainFromGoods);
        this.rows = [];
        this.addRow();
        this.rows[0].debit.acctNoWithDesc = glAccDebit.acctNo + '-' + glAccDebit.description;
        this.rows[0].debit.amount = amount;
        this.rows[0].debit.analyticPart = String('000');
        this.rows[0].credit.acctNoWithDesc = glAccCredit.acctNo + '-' + glAccCredit.description;
        this.rows[0].credit.amount = amount;
        this.rows[0].credit.analyticPart = String('000');
      });
    });
  }

  getGlAccountByAcctno(acctno: string): GlAccount {
    const account = this.accountsDebit.filter(acc => acc.acctNo === acctno);
    return account[0];
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  addRow() {
    const newRow = new GlWriteFeBoth();
    newRow.ident = this.identNext++;
    newRow.debit = new GlWriteFe();
    newRow.credit = new GlWriteFe();
    this.rows.push(newRow);
  }

  deleteRow() {
    if (this.selectedRow) {
      this.rows = this.rows.filter(row => row.ident !== this.selectedRow.ident);
      this.selectedRow = null;
    }
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  selectRow(glWrite: GlWriteFeBoth) {
    if (this.selectedRow && this.selectedRow.ident === glWrite.ident) {
      this.selectedRow = null;
    } else {
      this.selectedRow = glWrite;
    }
  }

  // editGlAccount(account: GlAccount) {
  //   this.router.navigate(['/edit-glaccount', account.id]);
  //   // this.accountsService.updateAccount(account).subscribe((resp) => {
  //   //   console.log('edit account');
  //   //   this.ngOnInit();
  //   // });
  // }
  //
  // createGlAccount() {
  //   this.router.navigate(['/edit-glaccount']);
  // }
  //
  // deleteGlAccount(account: GlAccount) {
  //   this.inventoryService.deleteAccountById(account.id).subscribe((resp) => {
  //     console.log('delete account');
  //     this.page = 0;
  //     this.ngOnInit();
  //   });
  // }
  makeAmortization(inventoryItem: InventoryItem) {
    let numberOfYears: number;
    switch (inventoryItem.depreciationClass) {
      case 'A':
        numberOfYears = 4;
        break;
      case 'B':
        numberOfYears = 10;
        break;
      case 'C':
        numberOfYears = 20;
        break;
      case 'D':
        numberOfYears = 50;
        break;
      default:
        console.error('Error while assigning years with depreciation class: ' + inventoryItem.depreciationClass);
        throw new Error('Error while assigning years with depreciation class: ' + inventoryItem.depreciationClass);
    }
    // todo accelerated
    const amortization = inventoryItem.price / (numberOfYears * 12);  // monthly todo yearly
    if (!inventoryItem.depreciationItems) {
      inventoryItem.depreciationItems = [];
    }
    const depreciationItem = new DepreciationItem();
    depreciationItem.newItem = true;
    depreciationItem.inventoryItemId = inventoryItem.id;
    depreciationItem.amount = amortization;
    depreciationItem.period = (new Date().getMonth() + 1).toString() + '/' + new Date().getFullYear().toString();
    inventoryItem.depreciationItems.push(depreciationItem);
    this.newDepreciationItems.push(depreciationItem);
    this.generateAccounting(amortization);
  }

  selectInventoryItem(inventoryItem: InventoryItem) {
    this.selectedInventoryItem = inventoryItem;
  }

  findAccountByAcctno(acctnoWithDesc: string, accounts: Array<GlAccount>): GlAccount {
    const strings = acctnoWithDesc.split('-');
    const acctno = strings[0].trim();
    const glAccounts = accounts.filter(account => account.acctNo === acctno);
    return glAccounts[0];
  }

  savedCalledAll() {
    const complexDepreciation = new ComplexDepreciation();
    complexDepreciation.depreciations = this.newDepreciationItems;

    const glWrites = [];
    this.rows.forEach(row => {
      if (row.debit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'DEBIT';
        glWriteDto.amount = row.debit.amount;
        glWriteDto.analyticPart = row.debit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.debit.acctNoWithDesc, this.accountsDebit).id;
        glWrites.push(glWriteDto);
      }
      if (row.credit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'CREDIT';
        glWriteDto.amount = row.credit.amount;
        glWriteDto.analyticPart = row.credit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.credit.acctNoWithDesc, this.accountsCredit).id;
        glWrites.push(glWriteDto);
      }
    });

    complexDepreciation.accounting = glWrites;

    console.log('Complex depreciation: ' + JSON.stringify(complexDepreciation));

    this.inventoryService.createDepreciationItem(complexDepreciation).subscribe(resp => {
      console.log(JSON.stringify(resp));
      console.log('Inventory items saved.');
      this.router.navigate(['/menu']);
    });
  }
}
