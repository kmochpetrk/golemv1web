import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CreateCompanyRooUserRequest, CreateCompanyRooUserResponse} from '../models/login';
import {CreateCompanyService} from '../create-company.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.css']
})
export class CreateCompanyComponent implements OnInit {
  createCompanyForm: FormGroup;
  response: CreateCompanyRooUserResponse;

  constructor(private formBuilder: FormBuilder, public createCompanyService: CreateCompanyService, private router: Router) {
    this.createCompanyForm = this.formBuilder.group({
      company: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
  }

  submitLoginForm() {
    if (this.createCompanyForm.valid) {
      const createCompanyRooUserRequest = new CreateCompanyRooUserRequest();
      createCompanyRooUserRequest.company = this.createCompanyForm.get('company').value;
      createCompanyRooUserRequest.username = this.createCompanyForm.get('username').value;
      createCompanyRooUserRequest.password = this.createCompanyForm.get('password').value;
      this.createCompanyService.createCompanyRootUser(createCompanyRooUserRequest).subscribe(createCompanyRooUserResponse => {
        this.response = createCompanyRooUserResponse;

        this.router.navigate(['/login']);
      });
    }
  }

  createCompanyWithRootUser() {
    this.router.navigate(['create-company']);
  }
}
