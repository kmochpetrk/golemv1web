import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputGoodsReceiptComponent } from './input-goods-receipt.component';

describe('InputGoodsReceiptComponent', () => {
  let component: InputGoodsReceiptComponent;
  let fixture: ComponentFixture<InputGoodsReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputGoodsReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputGoodsReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
