import {Component, OnDestroy, OnInit} from '@angular/core';
import {CrmItem, GlWriteFe, GlWriteFeBoth, GsItem} from '../models/gl-writes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlAccount} from '../models/accounts';
import {GoodsServices} from '../models/goods-services';
import {AccountService} from '../account.service';
import {GoodsServicesService} from '../goods-services.service';
import {CrmsService} from '../crms.service';
import {formatDate} from '@angular/common';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {GlWriteDto, GoodsInputReceiptRequest, WarehouseGoodsDto} from '../models/InputGoodsReceipt';
import {InputGoodsReceiptService} from '../input-goods-receipt.service';
import {WarehouseService} from '../warehouse.service';
import {Warehouse} from '../models/warehouses';

@Component({
  selector: 'app-input-goods-receipt',
  templateUrl: './input-goods-receipt.component.html',
  styleUrls: ['./input-goods-receipt.component.css']
})
export class InputGoodsReceiptComponent implements OnInit, OnDestroy {

  rows: Array<GlWriteFeBoth> = [];

  accountingForm: FormGroup;

  identNext = 0;
  identGsNext = 0;

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];
  selectedRow: GlWriteFeBoth;
  // documentTypes=['InputInvoice', 'OutputInvoice', 'cash_output_receipt', 'cash_input_receipt', 'Warehouse_Income', 'Warehouse_Outcome'];

  goodsServices: Array<GoodsServices> = [];

  gsItems: Array<GsItem> = [];

  crm: CrmItem[];
  warehouseName: string;
  warehouseId: number;
  warehouse: Warehouse;
  private sub: Subscription;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private goodsService: GoodsServicesService,
              private crmService: CrmsService, private activatedRoute: ActivatedRoute, private router: Router,
              private inputGoodsReceiptService: InputGoodsReceiptService, private warehouseService: WarehouseService) {
    this.accountingForm = this.formBuilder.group({
      issueDate: ['', [Validators.required]],
      crmCompany: ['', [Validators.required]],
      documentNumber: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {

    this.sub = this.activatedRoute.params.subscribe(params => {
      this.warehouseId = +params.warehouseId;
      this.warehouseName = params.warehouseName;
      this.warehouseService.getWarehouseById(this.warehouseId).subscribe(warehouse => {
        this.warehouse = warehouse;
      });
    });

    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.accountsDebit = accounts.accounts.content;
      this.accountsCredit = accounts.accounts.content;
    });
    this.goodsService.getGoodsServices(0, 100).subscribe(goodsServices => {
      this.goodsServices = goodsServices.goodsServices.content;
    });

    this.accountingForm.get('issueDate').setValue(formatDate(new Date(), 'yyyy-MM-dd', 'en'), {onlySelf: true});

    this.crmService.getCrms(0, 1000).subscribe(data => {
      this.crm = data.crms.content;
    });
    this.inputGoodsReceiptService.nextDocumentNumber().subscribe(resp => {
      this.accountingForm.get('documentNumber').setValue(resp.documentNumber, {
        onlySelf: true, emitEvent: false
      });
    });
  }

  addRow() {
    const newRow = new GlWriteFeBoth();
    newRow.ident = this.identNext++;
    newRow.debit = new GlWriteFe();

    newRow.credit = new GlWriteFe();
    this.rows.push(newRow);
  }

  deleteRow() {
    if (this.selectedRow) {
      this.rows = this.rows.filter(row => row.ident !== this.selectedRow.ident);
      this.selectedRow = null;
    }
  }

  selectRow(glWrite: GlWriteFeBoth) {
    if (this.selectedRow && this.selectedRow.ident === glWrite.ident) {
      this.selectedRow = null;
    } else {
      this.selectedRow = glWrite;
    }
  }

  submitAccountingForm() {
    const request = new GoodsInputReceiptRequest();
    const crmValue = this.accountingForm.get('crmCompany').value;
    const splitedCrm = (crmValue as string).split(':');
    request.crmId = Number(splitedCrm[1]);
    request.documentNumber = this.accountingForm.get('documentNumber').value;
    const glWrites = [];
    const goods = [];
    this.rows.forEach(row => {
      if (row.debit) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'DEBIT';
        glWriteDto.amount = row.debit.amount;
        glWriteDto.analyticPart = row.debit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.debit.acctNoWithDesc, this.accountsDebit).id;
        glWrites.push(glWriteDto);
      }
      if (row.credit) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'CREDIT';
        glWriteDto.amount = row.credit.amount;
        glWriteDto.analyticPart = row.credit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.credit.acctNoWithDesc, this.accountsCredit).id;
        glWrites.push(glWriteDto);
      }
    });
    this.gsItems.forEach(gsItem => {
      const warehouseGoodsDto = new WarehouseGoodsDto();
      warehouseGoodsDto.count = gsItem.count;
      warehouseGoodsDto.goodsServiceId = gsItem.id;
      warehouseGoodsDto.unitPrice = this.getUnitPrice(gsItem.id);
      warehouseGoodsDto.vatTotal = this.getVat(gsItem.id, gsItem.count);
      warehouseGoodsDto.priceTotal = this.getPrice(gsItem.id, gsItem.count);
      warehouseGoodsDto.warehouseId = this.warehouseId;
      goods.push(warehouseGoodsDto);
    });
    request.glWrites = glWrites;
    request.warehouseGoodsDtos = goods;
    console.log('REQUEST: '  + JSON.stringify(request));
    this.inputGoodsReceiptService.createINputGoodsReceipt(request).subscribe((resp) => {
      const id = this.warehouseId;
      const name = this.warehouseName;
      this.router.navigate(['/warehouse-goods', id, name]);
    });
  }

  findAccountByAcctno(acctnoWithDesc: string, accounts: Array<GlAccount>): GlAccount {
    const strings = acctnoWithDesc.split('-');
    const acctno = strings[0].trim();
    const glAccounts = accounts.filter(account => account.acctNo === acctno);
    return glAccounts[0];
  }

  blurDesc($event: FocusEvent) {
    console.log('FocusEventType: ' + $event.type);
    console.log('FocusEvent: ' + this.accountingForm.get('description').value);
  }

  addRowGs() {
    const gsItem = new GsItem();
    gsItem.ident = this.identGsNext++;
    this.gsItems.push(gsItem);
  }

  // calcPrice(rowsElement: GsItem): number {
  //   const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
  //   if (goodsServices1.length === 1 && rowsElement.count) {
  //     return goodsServices1[0].packetOutputPrice * rowsElement.count;
  //   } else {
  //     return 0;
  //   }
  // }
  //
  // calcVat(rowsElement: GsItem): number {
  //   const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
  //   if (goodsServices1.length === 1 && rowsElement.count) {
  //     return Math.round(goodsServices1[0].packetOutputPrice * rowsElement.count * (goodsServices1[0].vatPercents / 100) * 100) / 100;
  //   } else {
  //     return 0;
  //   }
  // }
  //
  // calcTotal(rowsElement: GsItem) {
  //   const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
  //   if (goodsServices1.length === 1 && rowsElement.count) {
  //     return Math.round(goodsServices1[0].packetOutputPrice * rowsElement.count *
  //       ((100 + goodsServices1[0].vatPercents) / 100) * 100) / 100;
  //   } else {
  //     return 0;
  //   }
  // }

  calcTotalPrice(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getUnitPrice(item.id) * item.count;
    });
    return Math.round(total * 100) / 100;
  }

  calcTotalVat(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getVat(item.id, item.count);
    });
    return Math.round(total * 100) / 100;
  }

  calcTotalTotal(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getPrice(item.id, item.count);
    });
    return Math.round(total * 100) / 100;
  }

  delRowGs(rowsElement: GsItem) {
    this.gsItems = this.gsItems.filter(elem => elem.ident !== rowsElement.ident);
  }

  changeCompany(e) {
    this.crmCompany.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get crmCompany() {
    return this.accountingForm.get('crmCompany');
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  getUnitPrice(id: number): number {
    if (id) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return whGoodsLocal[0].packetInputPrice;
    }
    return 0;
  }

  getVat(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round((((whGoodsLocal[0].vatPercents / 100) *
        whGoodsLocal[0].packetInputPrice) * count) * 100) / 100;
    }
    return 0;
  }

  getPrice(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round((whGoodsLocal[0].packetInputPrice * count) * 100) / 100;
    }
    return 0;
  }

  generateAccounting() {
    this.accountService.getAccountsByGroup('GOODS').subscribe(accounts => {
      console.log('Goods account goods: ' + accounts[0].acctNo);
      const accountGoodsAcctno = accounts[0].acctNo;
      this.accountService.getAccountsByGroup('GOODS_GET').subscribe(accountsGet => {
        console.log('Goods account get: ' + accountsGet[0].acctNo);
        const accountGoodsGetAcctno = accountsGet[0].acctNo;
        const glAccGoods = this.getGlAccountByAcctno(accountGoodsAcctno);
        const glAccGoodsGet = this.getGlAccountByAcctno(accountGoodsGetAcctno);
        const amount = this.calcTotalPrice();
        this.rows = [];
        this.addRow();
        this.rows[0].debit.acctNoWithDesc = glAccGoods.acctNo + '-' + glAccGoods.description;
        this.rows[0].debit.amount = amount;
        this.rows[0].debit.analyticPart = String(this.warehouse.analyticAccount);
        this.rows[0].credit.acctNoWithDesc = glAccGoodsGet.acctNo + '-' + glAccGoodsGet.description;
        this.rows[0].credit.amount = amount;
        this.rows[0].credit.analyticPart = String(this.warehouse.analyticAccount);
      });
    });
  }

  getGlAccountByAcctno(acctno: string): GlAccount {
    const account = this.accountsDebit.filter(acc => acc.acctNo === acctno);
    return account[0];
  }
}
