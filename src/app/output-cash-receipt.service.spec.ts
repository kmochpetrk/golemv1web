import { TestBed } from '@angular/core/testing';

import { OutputCashReceiptService } from './output-cash-receipt.service';

describe('OutputCashReceiptService', () => {
  let service: OutputCashReceiptService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OutputCashReceiptService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
