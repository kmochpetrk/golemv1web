import {Component, OnInit} from '@angular/core';
import {BankStatementRequest, InputInvoiceSmall, OutputInvoiceSmall} from '../models/invoices';
import {GlWriteFe, GlWriteFeBoth} from '../models/gl-writes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommandToPay, GlAccount} from '../models/accounts';
import {AccountService} from '../account.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BankStatementService} from '../bank-statement.service';
import {formatDate} from '@angular/common';
import {GlWriteDto} from '../models/InputGoodsReceipt';
import {InputInvoiceService} from '../input-invoice.service';
import {CommandToPayService} from '../command-to-pay.service';

@Component({
  selector: 'app-bank-statement-outcome',
  templateUrl: './bank-statement-outcome.component.html',
  styleUrls: ['./bank-statement-outcome.component.css']
})
export class BankStatementOutcomeComponent implements OnInit {

  inputInvoiceSmalls: InputInvoiceSmall[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  //
  commandToPays: CommandToPay[];
  selectedCommandToPay: CommandToPay;
  page1 = 0;
  size1 = 5;
  total1: number;
  pageNumbers1: number[];
  //

  bankAccounts = [];

  rows: Array<GlWriteFeBoth> = [];

  accountingForm: FormGroup;

  identNext = 0;

  selectedInvoice: InputInvoiceSmall;

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];
  selectedRow: GlWriteFeBoth;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private commandToPayService: CommandToPayService,
              private activatedRoute: ActivatedRoute, private router: Router,
              private bankStatementService: BankStatementService, private inputInvoiceService: InputInvoiceService) {
    this.accountingForm = this.formBuilder.group({
      issueDate: ['', [Validators.required]],
      documentNumber: ['', [Validators.required]],
      amountInput: ['', [Validators.required]],
      bankAccountInput: ['', [Validators.required]],
      variableSymbolInput: ['', [Validators.required]],
      bankStatementNumber: ['', [Validators.required]],
      ourBankAccount: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {

    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.accountsDebit = accounts.accounts.content;
      this.accountsCredit = accounts.accounts.content;
      this.bankAccounts = accounts.accounts.content.filter(acc => acc.glAccountGroupingType === 'BANK_ACCOUNTS').map(u => u.additionalInfo);
    });
    this.accountingForm.get('issueDate').setValue(formatDate(new Date(), 'yyyy-MM-dd', 'en'), {onlySelf: true});
    this.readInputInvoicesSmall();
    this.readCtp();
    this.bankStatementService.nextDocumentNumber().subscribe(resp => {
    this.accountingForm.get('documentNumber').setValue(resp.documentNumber, {
      onlySelf: true, emitEvent: false
    });
  });
}

private readInputInvoicesSmall() {
    this.inputInvoiceService.getInputInvoicesSmallNotPaid(this.page, this.size).subscribe((inputInvoicesSmallResponse) => {
      this.inputInvoiceSmalls = inputInvoicesSmallResponse.inputSmallInvoices.content;
      this.page = inputInvoicesSmallResponse.inputSmallInvoices.number;
      this.size = inputInvoicesSmallResponse.inputSmallInvoices.size;
      this.total = inputInvoicesSmallResponse.inputSmallInvoices.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  private readCtp() {
    this.commandToPayService.getCommandToPaysNotCtp(this.page1, this.size1).subscribe(commandToPaysResponse => {
      this.commandToPays = commandToPaysResponse.commandToPays.content;
      this.page1 = commandToPaysResponse.commandToPays.number;
      this.size1 = commandToPaysResponse.commandToPays.size;
      this.total1 = commandToPaysResponse.commandToPays.totalElements;
      const sizes = Math.ceil(this.total1 / this.size1);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page1 > 0) {
        pageNumbers.push(this.page1);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page1 + 1) < sizes))) {
          pageNumbers.push(this.page1 + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers1 = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.readInputInvoicesSmall();
  }

  readData1(page: number, size: number) {
    this.page1 = page;
    this.size1 = size;
    this.readCtp();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  addRow() {
    const newRow = new GlWriteFeBoth();
    newRow.ident = this.identNext++;
    newRow.debit = new GlWriteFe();

    newRow.credit = new GlWriteFe();
    this.rows.push(newRow);
  }

  deleteRow() {
    if (this.selectedRow) {
      this.rows = this.rows.filter(row => row.ident !== this.selectedRow.ident);
      this.selectedRow = null;
    }
  }

  selectRow(glWrite: GlWriteFeBoth) {
    if (this.selectedRow && this.selectedRow.ident === glWrite.ident) {
      this.selectedRow = null;
    } else {
      this.selectedRow = glWrite;
    }
  }

  submitAccountingForm() {
    const request = new BankStatementRequest();
    request.issueDate = this.accountingForm.get('issueDate').value;
    request.invoiceId = this.selectedInvoice?.id;
    request.ctpId = this.selectedCommandToPay?.id;
    request.variableSymbolFromInvoice = this.selectedInvoice?.variableSymbol;
    request.documentNumber = this.accountingForm.get('documentNumber').value;
    request.variableSymbolInput = this.accountingForm.get('variableSymbolInput').value;
    request.bankAccountInput = this.accountingForm.get('bankAccountInput').value;
    request.bankStatementNumber = this.accountingForm.get('bankStatementNumber').value;
    request.amountInput = this.accountingForm.get('amountInput').value;
    request.ourBankAccount = this.accountingForm.get('ourBankAccount').value.split(':')[1].trim();
    const glWrites = [];
    this.rows.forEach(row => {
      if (row.debit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'DEBIT';
        glWriteDto.amount = row.debit.amount;
        glWriteDto.analyticPart = row.debit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.debit.acctNoWithDesc, this.accountsDebit).id;
        glWrites.push(glWriteDto);
      }
      if (row.credit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'CREDIT';
        glWriteDto.amount = row.credit.amount;
        glWriteDto.analyticPart = row.credit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.credit.acctNoWithDesc, this.accountsCredit).id;
        glWrites.push(glWriteDto);
      }
    });
    request.glWrites = glWrites;
    request.income = false;
    console.log('REQUEST: '  + JSON.stringify(request));
    this.bankStatementService.createBankStatement(request).subscribe((resp) => {
      this.router.navigate(['/menu']);
    });
  }

  findAccountByAcctno(acctnoWithDesc: string, accounts: Array<GlAccount>): GlAccount {
    const strings = acctnoWithDesc.split('-');
    const acctno = strings[0].trim();
    const glAccounts = accounts.filter(account => account.acctNo === acctno);
    return glAccounts[0];
  }

  blurDesc($event: FocusEvent) {
    console.log('FocusEventType: ' + $event.type);
    console.log('FocusEvent: ' + this.accountingForm.get('description').value);
  }

  generateAccounting() {
    const totalPrice = this.selectedInvoice ? this.selectedInvoice.amount :
      (this.selectedCommandToPay ? this.selectedCommandToPay.commandToPayItems[0].amount : 0);
    this.accountService.getAccountsByGroup(this.selectedInvoice ? 'TRADE_OTHER_PAYABLES' :
      ((this.selectedCommandToPay && this.selectedCommandToPay.commandToPayItems[0].variableSymbol.startsWith('Emp')) ? 'EMPLOYEES' :
        'TRADE_OTHER_PAYABLES')).subscribe(tap => {
      console.log('trade other payables: ' + tap[0].acctNo);
      const tradeOtherPayables = tap[0].acctNo;
      this.accountService.getAccountsByGroup('BANK_ACCOUNTS').subscribe(eg => {
        console.log('bank account: ' + eg[0].acctNo);
        const bankAccountAcctno = eg[0].acctNo;
        const glDebit = this.getGlAccountByAcctno(tradeOtherPayables);
        const glCredit = this.getGlAccountByAcctno(bankAccountAcctno);
        this.rows = [];
        this.addRow();
        this.rows[0].debit.acctNoWithDesc = glDebit.acctNo + '-' + glDebit.description;
        this.rows[0].debit.amount = totalPrice;
        this.rows[0].debit.analyticPart = String('000');
        this.rows[0].credit.acctNoWithDesc = glCredit.acctNo + '-' + glCredit.description;
        this.rows[0].credit.amount = totalPrice;
        this.rows[0].credit.analyticPart = String('000');
      });
    });
  }

  getGlAccountByAcctno(acctno: string): GlAccount {
    const account = this.accountsDebit.filter(acc => acc.acctNo === acctno);
    return account[0];
  }

  toggle(inputInvoiceSmall: InputInvoiceSmall) {
    if (!inputInvoiceSmall.selected) {
      this.selectedInvoice = inputInvoiceSmall;
      // other processing
      this.accountingForm.get('bankAccountInput').setValue(inputInvoiceSmall.otherBankAccount, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('amountInput').setValue(inputInvoiceSmall.amount, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('variableSymbolInput').setValue(inputInvoiceSmall.variableSymbol, {
        onlySelf: true, emitEvent: false
      });
    } else {
      this.selectedInvoice = null;
      // other processing
      this.accountingForm.get('bankAccountInput').setValue(null, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('amountInput').setValue(null, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('variableSymbolInput').setValue(null, {
        onlySelf: true, emitEvent: false
      });
    }
    inputInvoiceSmall.selected = !inputInvoiceSmall.selected;
  }

  toggleCtp(commandToPay: CommandToPay) {
    if (!commandToPay.selected) {
      this.selectedCommandToPay = commandToPay;
      // other processing
      this.accountingForm.get('bankAccountInput').setValue(commandToPay.commandToPayItems[0].otherBankAccount, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('amountInput').setValue(commandToPay.commandToPayItems[0].amount, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('variableSymbolInput').setValue(commandToPay.commandToPayItems[0].variableSymbol, {
        onlySelf: true, emitEvent: false
      });
    } else {
      this.selectedCommandToPay = null;
      // other processing
      this.accountingForm.get('bankAccountInput').setValue(null, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('amountInput').setValue(null, {
        onlySelf: true, emitEvent: false
      });
      this.accountingForm.get('variableSymbolInput').setValue(null, {
        onlySelf: true, emitEvent: false
      });
    }
    commandToPay.selected = !commandToPay.selected;
  }


  changeBankAccount(e) {
    this.ourBankAccount.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get ourBankAccount() {
    return this.accountingForm.get('ourBankAccount');
  }
}
