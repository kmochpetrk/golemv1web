import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankStatementOutcomeComponent } from './bank-statement-outcome.component';

describe('BankStatementOutcomeComponent', () => {
  let component: BankStatementOutcomeComponent;
  let fixture: ComponentFixture<BankStatementOutcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankStatementOutcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankStatementOutcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
