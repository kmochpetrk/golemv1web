import { TestBed } from '@angular/core/testing';

import { WgGoodsService } from './wg-goods.service';

describe('WgGoodsService', () => {
  let service: WgGoodsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WgGoodsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
