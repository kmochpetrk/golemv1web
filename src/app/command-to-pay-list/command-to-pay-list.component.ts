import {Component, OnInit} from '@angular/core';
import {CommandToPay} from '../models/accounts';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';
import {CommandToPayService} from '../command-to-pay.service';

@Component({
  selector: 'app-command-to-pay-list',
  templateUrl: './command-to-pay-list.component.html',
  styleUrls: ['./command-to-pay-list.component.css']
})
export class CommandToPayListComponent implements OnInit {
  commandToPays: CommandToPay[];
  selectedCommandToPay: CommandToPay;
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  constructor(public authenticationService: AuthenticationService,
              private commandToPayService: CommandToPayService, private router: Router) { }

  ngOnInit(): void {
    this.commandToPayService.getCommandToPays(this.page, this.size).subscribe(commandToPaysResponse => {
      this.commandToPays = commandToPaysResponse.commandToPays.content;
      this.page = commandToPaysResponse.commandToPays.number;
      this.size = commandToPaysResponse.commandToPays.size;
      this.total = commandToPaysResponse.commandToPays.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  selectCommandToPay(commandToPay: CommandToPay) {
    this.selectedCommandToPay = commandToPay;
  }
}

