import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandToPayListComponent } from './command-to-pay-list.component';

describe('CommandToPayListComponent', () => {
  let component: CommandToPayListComponent;
  let fixture: ComponentFixture<CommandToPayListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandToPayListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandToPayListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
