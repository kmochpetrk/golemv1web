import {Warehouse} from './warehouses';
import {GoodsServices} from './goods-services';


export interface GoodsInput {
  id: number;
  numberOfPackets: number;
  inputPrice: number;
  timeIn: Date;
}

export interface WhGoods {
  id: number;
  warehouse: Warehouse;
  goodsServices: GoodsServices;
  goodsInputs: GoodsInput[];
  inputPrice: number;
  outputPrice: number;
  count: number;
}

export class WhGoodses {
  content: WhGoods[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export interface WhGoodsResponse {
  warehouseGoodsServices: WhGoodses;
}


