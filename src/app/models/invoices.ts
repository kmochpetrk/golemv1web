import {GlWriteDto, WarehouseGoodsDto} from './InputGoodsReceipt';
import {Crm} from './crms';

export class OutputInvoiceRequest {
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  GORIds: Array<number>;
  issueDate: Date;
  payDate: Date;
  documentNumber: string;
  crmId: number;
  paymentType: string;
  transportType: string;
}

export class OutputInvoiceResponse {
  id: number;
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  GORIds: Array<number>;
  issueDate: Date;
  payDate: Date;
  documentNumber: string;
  crmId: number;
}

export class InputInvoiceRequest {
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  gorIds: Array<number>;
  issueDate: Date;
  variableSymbol: string;
  payDate: Date;
  documentNumber: string;
  crmId: number;
  paymentType: string;
  transportType: string;
  bankAccount: string;
}

export class InputCashReceiptRequest {
  id: number;
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  issueDate: Date;
  cashierTitle: string;
  documentNumber: string;
  gorIds: Array<number>;
}

export class InputCashReceiptResponse {
  id: number;
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  issueDate: Date;
  cashierTitle: string;
  documentNumber: string;
}

export class OutputCashReceiptRequest {
  id: number;
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  issueDate: Date;
  cashierTitle: string;
  documentNumber: string;
  GORIds: Array<number>;
}

export class OutputCashReceiptResponse {
  id: number;
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  issueDate: Date;
  cashierTitle: string;
  documentNumber: string;
}

export class BankStatementRequest {
  glWrites: GlWriteDto[];
  issueDate: Date;
  variableSymbolFromInvoice: string;
  variableSymbolInput: string;
  amountInput: number;
  bankAccountInput: string;
  bankStatementNumber: string; // from bank original same for more BS
  documentNumber: string;
  invoiceId: number;
  ctpId: number;
  ourBankAccount: string;
  income: boolean;
}

export class OutputInvoiceSmall {
  issueDate: Date;
  variableSymbol: string;
  crmName: string;
  amount: number;
  id: number;
  selected = false;
}

export class InputInvoiceSmall {
  issueDate: Date;
  variableSymbol: string;
  crmName: string;
  amount: number;
  id: number;
  selected = false;
  otherBankAccount: string;
  documentNumber: string;
}

/// outputGoodsReceipt

export interface GlAccount {
  id: number;
  acctNo: string;
  glAccountGroupingType?: any;
  glAccountType: string;
  glAccountSideType: string;
  description: string;
  accountingUnit?: any;
}

export interface GlWrite {
  id: number;
  amount: number;
  glAccount: GlAccount;
  analyticPart?: any;
  glAccountSideType: string;
}

export interface PrimaryAddress {
  id: number;
  street: string;
  descNumber: string;
  city: string;
  zipCode: string;
}

export interface AccountingUnit {
  id: number;
  name: string;
  ico: string;
  dic: string;
  nextGoodsOutputReceiptNumber: number;
  version: number;
  outputInvoiceNumber: number;
  primaryAddress: PrimaryAddress;
}

export interface Warehouse {
  id: number;
  name: string;
  address?: any;
  analyticAccount: number;
}

export interface GoodsServices {
  id: number;
  name: string;
  unit: string;
  gsType?: any;
  packetOutputPrice: number;
  packetInputPrice: number;
  vatPercents: number;
  numberOfUnitsInPacket: number;
}

export interface GoodsInput {
  id: number;
  numberOfPackets: number;
  inputPrice: number;
  timeIn: Date;
}

export interface GoodsOutput {
  id: number;
  numberOfPackets: number;
  outputPrice: number;
  timeIn: Date;
}

export interface WarehouseGoodsServices {
  id: number;
  warehouse: Warehouse;
  goodsServices: GoodsServices;
  goodsInputs: GoodsInput[];
  goodsOutputs: GoodsOutput[];
  inputPrice?: any;
  outputPrice?: any;
  count?: any;
}

export interface GoodsServiceItem {
  id: number;
  warehouseGoodsServices: WarehouseGoodsServices;
  unitPrice?: any;
  numberOfUnits: number;
  vatPercent: number;
  vatTotal: number;
  priceTotal?: any;
}

export class OutputGoodsReceipt {
  id: number;
  glWrites: GlWrite[];
  accountingDate: string;
  taxDate?: any;
  writtenBy?: any;
  goodsServiceItems: GoodsServiceItem[];
  documentNumber?: any;
  crm: Crm;
  selected = false;
}

export class InputGoodsReceipt {
  id: number;
  glWrites: GlWrite[];
  accountingDate: string;
  taxDate?: any;
  writtenBy?: any;
  goodsServiceItems: GoodsServiceItem[];
  documentNumber?: any;
  crm: Crm;
  selected = false;
}

export class GoodsOutputReceipts {
  content: OutputGoodsReceipt[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class GoodsInputReceipts {
  content: InputGoodsReceipt[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class OutputSmallInvoices {
  content: OutputInvoiceSmall[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class InputSmallInvoices {
  content: InputInvoiceSmall[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export interface GoodsOutputReceiptsResponse {
  goodsOutputReceipts: GoodsOutputReceipts;
}

export interface GoodsInputReceiptsResponse {
  goodsInputReceipts: GoodsInputReceipts;
}

export interface OutputInvoicesSmallResponse {
  outputSmallInvoices: OutputSmallInvoices;
}

export interface InputInvoicesSmallResponse {
  inputSmallInvoices: InputSmallInvoices;
}
