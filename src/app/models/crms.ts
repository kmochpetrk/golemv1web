import {GlWriteFeBoth} from './gl-writes';

export class Crms {
  content: Crm[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class Crm {
  id: number;
  name: string;
  dic: string;
  ico: string;
  primaryAddress: Address;
  crmType: string;
  phone: string;
  email: string;
  bankAccount: string;
}

export class InventoryItem {
  id: number;
  name: string;
  inventoryNumber: string;
  startDate: Date;
  endDate: Date;
  depreciationType: string;
  depreciationClass: string;
  propertyKind: string;
  price: number;
  depreciationItems: Array<DepreciationItem>;
}

export class Employee {
  id: number;
  firstname: string;
  surname: string;
  employeeNumber: string;
  startDate: Date;
  endDate: Date;
  monthlyGrossSalary: number;
  birthNumber: string;
  birthDate: Date;
  monthlySalaries: Array<MonthlySalaryItem>;
  personalBankAccount: string;
}

export class DepreciationItem {
  id: number;
  period: string;
  amount: number;
  newItem = false;
  inventoryItemId: number;
  bookingNumber: string;
}

export class MonthlySalaryItem {
  id: number;
  period: string;
  amount: number;
  newItem = false;
  employeeId: number;
  bookingNumber: string;
}

export class ComplexMonthlySalary {
  monthlySalaryDtos: MonthlySalaryItem[];
  accounting: GlWriteFeBoth[];
}


export class ComplexDepreciation {
  depreciations: DepreciationItem[];
  accounting: GlWriteFeBoth[];
}

export interface CrmsResponse {
  crms: Crms;
}

export class Address {
  street: string;
  descNumber: string;
  city: string;
  zipCode: string;
}
