export class GlWriteDto {
  glAccountSideType: string;
  glAccountId: number;
  amount: number;
  analyticPart: string;
  acctNo: string;
}

export class WarehouseGoodsDto {
  count: number;
  goodsServiceId: number;
  unitPrice: number;
  vatTotal: number;
  priceTotal: number;
  warehouseId: number;
}

export class GoodsInputReceiptRequest {
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  crmId: number;
  documentNumber: string;
}

export class GoodsOutputReceiptRequest {
  glWrites: GlWriteDto[];
  warehouseGoodsDtos: WarehouseGoodsDto[];
  crmId: number;
  documentNumber: string;
}
