export interface Word {
  id: number;
  word: string;
  otherLanguagesWordSet: any[];
}

export interface RuleElement {
  id: number;
  percents: number;
  glAccountSideType: string;
  accountGroupingType: string;
}

export interface AccRule {
  id: number;
  ruleElements: RuleElement[];
  glWholeWriteType: string;
}

export interface SentenceSaved {
  id: number;
  words: Word[];
  accRule: AccRule;
}

export interface GeneralAccountingTrainResponse {
  trained: boolean;
  sentenceSaved: SentenceSaved;
}

export interface GeneralAccountingPredictResponse {
  rule: AccRule;
}
