import {GlWriteDto} from './InputGoodsReceipt';
import {Address, Employee, InventoryItem} from './crms';

export class Accounts {
  content: GlAccount[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class InventoryItems {
  content: InventoryItem[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class EmployeeItems {
  content: Employee[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class GlAccount {
  id: number;
  acctNo: string;
  glAccountType: string;
  glAccountSideType: string;
  description: string;
  glAccountGroupingType: string;
  additionalInfo: string;
}

export interface AccountsResponse {
  accounts: Accounts;
}

export interface EmployeesResponse {
  employees: EmployeeItems;
}

export interface InventoryItemsResponse {
  inventoryItems: InventoryItems;
}

export class Booking {
  id: number;
  glWrites: GlWriteDto[];
  glWholeWriteType: string;
  accountingDate: Date;
  taxDate: Date;
  writtenBy: string;
  crm: string;
  documentNumber: string;
  cancelled: boolean;
  descriptionOfCase: string;
}

export class InternalOrder {
  id: number;
  paymentType: string;
  deliveryType: string;
  totalVat: number;
  totalPrice: number;
  oinumber: string;
  ourBankAccount: string;
  company: string;
  ordersItems: OrderItem[];
  crmName: string;
  crmIco: string;
  crmId: string;
  address: Address;
}

export class OrderItem {
  id: number;
  goodsServicesName: string;
  goodsServicesNumber: string;
  goodsServicesId: number;
  orderedCount: number;
  totalItemVat: number;
  totalItemPrice: number;
  warehouseName: string;
  warehouseId: number;
  warehouseAddress: string;
  unitOutputPrice: number;
  unitOutputPriceWithVat: number;
  vatPercent: number;
}

export class InputInvoiceNotPaid {
  issueDate: string;
  variableSymbol: string;
  crmName: string;
  id: number;
  paymentType?: any;
  transportType?: any;
  payDate: string;
  total?: any;
  totalTax?: any;
  warehouseGoodsServicesDtos: WarehouseGoodsServicesDto[];
  bsId?: any;
  gorids: number[];
}

export class OutputInvoiceNotPaid {
  issueDate: string;
  variableSymbol: string;
  crmName: string;
  id: number;
  paymentType?: any;
  transportType?: any;
  payDate: string;
  total?: any;
  totalTax?: any;
  warehouseGoodsServicesDtos: WarehouseGoodsServicesDto[];
  bsId?: any;
  gorids: number[];
}

export class InputInvoicesNotPaid {
  content: InputInvoiceNotPaid[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class OutputInvoicesNotPaid {
  content: OutputInvoiceNotPaid[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export interface WarehouseGoodsServicesDto {
  warehouseId?: any;
  warehouseName?: any;
  goodsServicesId: number;
  vatTotal: number;
  amountTotal: number;
  numberOfUnits: number;
  goodsServicesName: string;
}

export class Bookings {
  content: Booking[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class CommandToPays {
  content: CommandToPay[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class CommandToPay {
  id: number;
  documentNumber: string;
  ourBankAccount: string;
  commandToPayItems: CommandToPayItem[];
  selected = false;
}

export class CommandToPayItem {
  id: number;
  variableSymbol: string;
  messageForReceiver: string;
  otherBankAccount: string;
  specificSymbol: string;
  constantSymbol: string;
  amount: number;
  ident: number;
}

export class InternalOrders {
  content: InternalOrder[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export interface CommandToPaysResponse {
  commandToPays: CommandToPays;
}

export interface BookingsResponse {
  bookings: Bookings;
}

export interface InputInvoicesNotPaidResponse {
  inputMediumInvoices: InputInvoicesNotPaid;
}

export interface OutputInvoicesNotPaidResponse {
  outputMediumInvoices: OutputInvoicesNotPaid;
}

export interface InternalOrdersResponse {
  internalOrders: InternalOrders;
}

export interface BookingResponse {
  booking: Booking;
}

export class BalanceSheetItem {
  acctNo: string;
  debit: number;
  credit: number;
}

export class ProfitLossItem {
  acctNo: string;
  debit: number;
  credit: number;
}

export interface ProfitLossResponse {
  profitAndLoss: ProfitLossItem[];
}

export interface BalanceSheetResponse {
  balance: BalanceSheetItem[];
}
