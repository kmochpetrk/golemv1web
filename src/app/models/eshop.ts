import {Address, Crm} from './crms';

export class Item {
  warehouseId: number;
  whName: string;
  address?: Address;
  count: number;
  unitOutputPrice: number;
  unitOutputPriceWithVat: number;
  vatPercent: number;
  orderedCount?: number;
  totalItemVat ?: number;
  totalItemPrice ?: number;
}

export class EshopGoodsService {
  goodsServiceId: number;
  goodsServiceNumber: string;
  name: string;
  items: Item[];
}

export class OrderFull {
  eshopGoodsService: EshopGoodsService;
  item: Item;
}

export class OrderFullForAcc {
  oINumber: string;
  orders: OrderFull[];
  crm: Crm;
  paymentType: string;
  deliveryType: string;
  totalVat ?: number;
  totalPrice ?: number;
  ourBankAccount: string;
  company: string;
}

export interface Sort {
  sorted: boolean;
  unsorted: boolean;
  empty: boolean;
}

export interface Pageable {
  sort: Sort;
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  unpaged: boolean;
}

export interface Sort2 {
  sorted: boolean;
  unsorted: boolean;
  empty: boolean;
}

export interface GoodsServices {
  content: EshopGoodsService[];
  pageable: Pageable;
  totalElements: number;
  totalPages: number;
  last: boolean;
  size: number;
  number: number;
  sort: Sort2;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

export interface EshopProductPageResponse {
  goodsServices: GoodsServices;
}

export class Order {
  goodsServiceId: number;
  item: Item;
  count: number;
}

export class CompaniesResponse {
  companies: Array<string>;
}

export interface BankAccountResponse {
  bankAccount: string;
}
