import {Address} from './crms';

export class Warehouses {
  content: Warehouse[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class Warehouse {
  id: number;
  name: string;
  address: Address;
  analyticAccount: number;
}

export interface WarehousesResponse {
  warehouses: Warehouses;
}
