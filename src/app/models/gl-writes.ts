import {GlAccount} from './accounts';
import {strict} from 'assert';
import {stringify} from 'querystring';

export class GlWrite {
  id: number;
  amount: number;
  glAccountSideType: string;
  glAccount: GlAccount;
}

export class GlWrites {
  debit: GlWrite;
  credit: GlWrite;
}

export class GlWriteFe {
  acctNoWithDesc: string;
  amount: number;
  analyticPart: string;
  state = 'new';
  multiplier ?: number;
}

export class GlWriteFeBoth {
  debit: GlWriteFe;
  credit: GlWriteFe;
  ident: number;
}

export class GsItem {
  id: number;
  ident: number;
  name: string;
  count: number;
  warehouseId ?: number;
}

export class CrmItem {
  id: number;
  name: string;
}
