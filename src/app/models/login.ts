export class LoginRequest {
  username: string;
  password: string;
}

export interface LoginResponse {
  jwtToken: string;
}

export class CreateCompanyRooUserRequest {
  company: string;
  username: string;
  password: string;
}

export class CreateCompanyRooUserResponse {
  company: string;
  username: string;
  password: string;
}
