export class GoodsServicesContent {
  content: GoodsServices[];
  totalPages: number;
  size: number;
  number: number;
  totalElements: number;
}

export class GoodsServices {
  id: number;
  name: string;
  unit: string;
  gsType?: any;
  packetOutputPrice: number;
  packetInputPrice: number;
  vatPercents: number;
  numberOfUnitsInPacket: number;
  goodsServicesNumber: string;
}

export interface GoodsServicesResponse {
  goodsServices: GoodsServicesContent;
}
