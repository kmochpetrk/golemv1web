import {GlWriteDto, WarehouseGoodsDto} from './InputGoodsReceipt';
import {GsItem} from './gl-writes';

export class RuleElement {
  glAccountSideType: string;
  accountGroupingType: string;
  percents: number;
}

export class Rule {
  glWholeWriteType: string;
  ruleElements: RuleElement[];
}

export class GeneralAccountingTrainRequest {
  sentence: string;
  glWholeWriteType: string;
  rule: Rule;
}

export class GeneralAccountingPostRequest {
  issueDate: Date;
  vatDate: Date;
  payDate: Date;
  glWholeWriteType: string;
  description: string;
  crmId: number;
  warehouseGoodsDtos: WarehouseGoodsDto[];
  glWrites: GlWriteDto[];
  variableSymbol: string;
}

export class GeneralAccountingPredictRequest {
  sentence: string;
  glWholeWriteType: string;
}
