import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';
import {ComplexMonthlySalary, Employee, MonthlySalaryItem} from '../models/crms';
import {EmployeeService} from '../employee.service';
import {GlAccount} from '../models/accounts';
import {GlWriteFe, GlWriteFeBoth} from '../models/gl-writes';
import {GlWriteDto} from '../models/InputGoodsReceipt';
import {AccountService} from '../account.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];

  rows: Array<GlWriteFeBoth> = [];
  selectedRow: GlWriteFeBoth;
  identNext = 0;
  selectedEmployee: Employee;
  newMonthlySalariesItems = [];


  constructor(public authenticationService: AuthenticationService, private accountService: AccountService,
              private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.employeeService.getEmployeeItems(this.page, this.size).subscribe(employeesResponse => {
      this.employees = employeesResponse.employees.content;
      this.page = employeesResponse.employees.number;
      this.size = employeesResponse.employees.size;
      this.total = employeesResponse.employees.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.accountsDebit = accounts.accounts.content;
      this.accountsCredit = accounts.accounts.content;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  // editGlAccount(account: GlAccount) {
  //   this.router.navigate(['/edit-glaccount', account.id]);
  //   // this.accountsService.updateAccount(account).subscribe((resp) => {
  //   //   console.log('edit account');
  //   //   this.ngOnInit();
  //   // });
  // }
  //
  // createGlAccount() {
  //   this.router.navigate(['/edit-glaccount']);
  // }
  //
  // deleteGlAccount(account: GlAccount) {
  //   this.employeeService.deleteAccountById(account.id).subscribe((resp) => {
  //     console.log('delete account');
  //     this.page = 0;
  //     this.ngOnInit();
  //   });
  // }

  generateAccounting(amount: number) {
    this.accountService.getAccountsByGroup('EXPENSES_EMPLOYEES').subscribe(tap => {
      console.log('amortization: ' + tap[0].acctNo);
      const expenseEmployees = tap[0].acctNo;
      this.accountService.getAccountsByGroup('EMPLOYEES').subscribe(eg => {
        console.log('depreciation: ' + eg[0].acctNo);
        const employees = eg[0].acctNo;
        const glAccDebit = this.getGlAccountByAcctno(expenseEmployees);
        const glAccCredit = this.getGlAccountByAcctno(employees);
        this.rows = [];
        this.addRow();
        this.rows[0].debit.acctNoWithDesc = glAccDebit.acctNo + '-' + glAccDebit.description;
        this.rows[0].debit.amount = amount;
        this.rows[0].debit.analyticPart = String('000');
        this.rows[0].credit.acctNoWithDesc = glAccCredit.acctNo + '-' + glAccCredit.description;
        this.rows[0].credit.amount = amount;
        this.rows[0].credit.analyticPart = String('000');
      });
    });
  }

  getGlAccountByAcctno(acctno: string): GlAccount {
    const account = this.accountsDebit.filter(acc => acc.acctNo === acctno);
    return account[0];
  }

  addRow() {
    const newRow = new GlWriteFeBoth();
    newRow.ident = this.identNext++;
    newRow.debit = new GlWriteFe();
    newRow.credit = new GlWriteFe();
    this.rows.push(newRow);
  }

  deleteRow() {
    if (this.selectedRow) {
      this.rows = this.rows.filter(row => row.ident !== this.selectedRow.ident);
      this.selectedRow = null;
    }
  }

  selectRow(glWrite: GlWriteFeBoth) {
    if (this.selectedRow && this.selectedRow.ident === glWrite.ident) {
      this.selectedRow = null;
    } else {
      this.selectedRow = glWrite;
    }
  }

  // editGlAccount(account: GlAccount) {
  //   this.router.navigate(['/edit-glaccount', account.id]);
  //   // this.accountsService.updateAccount(account).subscribe((resp) => {
  //   //   console.log('edit account');
  //   //   this.ngOnInit();
  //   // });
  // }
  //
  // createGlAccount() {
  //   this.router.navigate(['/edit-glaccount']);
  // }
  //
  // deleteGlAccount(account: GlAccount) {
  //   this.inventoryService.deleteAccountById(account.id).subscribe((resp) => {
  //     console.log('delete account');
  //     this.page = 0;
  //     this.ngOnInit();
  //   });
  // }
  makeSalary(employee: Employee) {
    const salary = employee.monthlyGrossSalary;
    if (!employee.monthlySalaries) {
      employee.monthlySalaries = [];
    }
    const monthlySalaryItem = new MonthlySalaryItem();
    monthlySalaryItem.newItem = true;
    monthlySalaryItem.employeeId = employee.id;
    monthlySalaryItem.amount = salary;
    monthlySalaryItem.period = (new Date().getMonth() + 1).toString() + '/' + new Date().getFullYear().toString();
    employee.monthlySalaries.push(monthlySalaryItem);
    this.newMonthlySalariesItems.push(monthlySalaryItem);
    this.generateAccounting(salary);
  }

  selectEmployee(employee: Employee) {
    this.selectedEmployee = employee;
  }

  findAccountByAcctno(acctnoWithDesc: string, accounts: Array<GlAccount>): GlAccount {
    const strings = acctnoWithDesc.split('-');
    const acctno = strings[0].trim();
    const glAccounts = accounts.filter(account => account.acctNo === acctno);
    return glAccounts[0];
  }

  savedCalledAll() {
    const complexMonthlySalary = new ComplexMonthlySalary();
    complexMonthlySalary.monthlySalaryDtos = this.newMonthlySalariesItems;

    const glWrites = [];
    this.rows.forEach(row => {
      if (row.debit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'DEBIT';
        glWriteDto.amount = row.debit.amount;
        glWriteDto.analyticPart = row.debit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.debit.acctNoWithDesc, this.accountsDebit).id;
        glWrites.push(glWriteDto);
      }
      if (row.credit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'CREDIT';
        glWriteDto.amount = row.credit.amount;
        glWriteDto.analyticPart = row.credit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.credit.acctNoWithDesc, this.accountsCredit).id;
        glWrites.push(glWriteDto);
      }
    });

    complexMonthlySalary.accounting = glWrites;

    console.log('Complex depreciation: ' + JSON.stringify(complexMonthlySalary));

    this.employeeService.createMonthlySalaryItem(complexMonthlySalary).subscribe(resp => {
      console.log(JSON.stringify(resp));
      console.log('Employee salary saved.');
      this.router.navigate(['/menu']);
    });
  }
}
