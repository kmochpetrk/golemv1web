import { Injectable } from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {
  GoodsInputReceiptsResponse,
  GoodsOutputReceiptsResponse,
  InputInvoiceRequest, InputInvoicesSmallResponse,
  OutputInvoiceRequest,
  OutputInvoicesSmallResponse
} from './models/invoices';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';

@Injectable()
export class InputInvoiceService {


  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createInputInvoice(inputInvoiceRequest: InputInvoiceRequest): Observable<any> {
    return this.http.post<any>(environment.baseUrl +
      '/inputinvoice', inputInvoiceRequest,  this.authService.createOptions());
  }

  public getInputGoodsReceiptNotInvoiced(page: number, size: number): Observable<GoodsInputReceiptsResponse> {
    return this.http.get<GoodsInputReceiptsResponse>(environment.baseUrl +
      '/goodsinputreceipt/notinvoiced?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/inputinvoice/nextnumber', this.authService.createOptions());
  }

  public getInputInvoicesSmall(page: number, size: number): Observable<InputInvoicesSmallResponse> {
    return this.http.get<InputInvoicesSmallResponse>(environment.baseUrl +
        '/inputinvoice/small?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public getInputInvoicesSmallNotPaid(page: number, size: number): Observable<InputInvoicesSmallResponse> {
    return this.http.get<InputInvoicesSmallResponse>(environment.baseUrl +
      '/inputinvoice/small2/on-command?page=' + page + '&size=' + size, this.authService.createOptions());
  }
}
