import { TestBed } from '@angular/core/testing';

import { InputCashReceiptService } from './input-cash-receipt.service';

describe('InputCashReceiptService', () => {
  let service: InputCashReceiptService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InputCashReceiptService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
