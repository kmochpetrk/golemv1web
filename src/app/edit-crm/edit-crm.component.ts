import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {CrmsService} from '../crms.service';
import {Address, Crm} from '../models/crms';

@Component({
  selector: 'app-edit-crm',
  templateUrl: './edit-crm.component.html',
  styleUrls: ['./edit-crm.component.css']
})
export class EditCrmComponent implements OnInit, OnDestroy {
  crmTypes = ['SUPPLIER', 'CUSTOMER', 'BOTH'];
  // glAccountSideTypes = ['DEBIT', 'CREDIT'];
  editCrmForm: FormGroup;

  readCrm: Crm;

  // groupPlDeb;
  // groupPlCred;
  // groupBalDeb;
  // groupBalCred;
  // groupModel = [];


  private sub: Subscription;
  id: number;

  constructor(private formBuilder: FormBuilder, public activatedRoute: ActivatedRoute,
              private crmsService: CrmsService, private router: Router) {
    this.editCrmForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      ico: ['', [Validators.required]],
      dic: ['', [Validators.required]],
      crmType: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      street: ['', [Validators.required]],
      descNumber: ['', [Validators.required]],
      city: ['', [Validators.required]],
      zipCode: ['', [Validators.required]],
      bankAccount: ['', [Validators.required]]
    });
    // this.editGlAccountForm.get('glAccountType').valueChanges.subscribe((val) => {
    //   this.initData(false);
    // });
    // this.editGlAccountForm.get('glAccountSideType').valueChanges.subscribe((val) => {
    //   this.initData(false);
    // });
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params.id;
      if (this.id) {
        this.crmsService.getCrmById(this.id).subscribe((crm) => {
          this.readCrm = crm;
          this.editCrmForm.get('name').setValue(crm.name, {
            onlySelf: true, emitEvent: false
          });
          this.editCrmForm.get('ico').setValue(crm.ico, {
            onlySelf: true, emitEvent: false
          });
          this.editCrmForm.get('dic').setValue(crm.dic, {
            onlySelf: true, emitEvent: false
          });
          this.editCrmForm.get('email').setValue(crm.email, {
            onlySelf: true, emitEvent: false
          });
          this.editCrmForm.get('phone').setValue(crm.phone, {
            onlySelf: true, emitEvent: false
          });
          this.editCrmForm.get('bankAccount').setValue(crm.bankAccount, {
            onlySelf: true, emitEvent: false
          });
          if (crm.primaryAddress) {
            this.editCrmForm.get('street').setValue(crm.primaryAddress.street, {
              onlySelf: true, emitEvent: false
            });
            this.editCrmForm.get('descNumber').setValue(crm.primaryAddress.descNumber, {
              onlySelf: true, emitEvent: false
            });
            this.editCrmForm.get('city').setValue(crm.primaryAddress.city, {
              onlySelf: true, emitEvent: false
            });
            this.editCrmForm.get('zipCode').setValue(crm.primaryAddress.zipCode, {
              onlySelf: true, emitEvent: false
            });
          }
          const crmType = this.crmTypes.indexOf(crm.crmType) + ': ' + crm.crmType;
          this.editCrmForm.get('crmType').setValue(crmType, {
            onlySelf: true, emitEvent: false
          });
        });
      }
    });
  }

  submitCrmForm() {
    this.editCrmForm.updateValueAndValidity();
    if (this.editCrmForm.valid) {
      const crm = new Crm();
      if (this.id) {
        crm.id = this.id;
      }
      crm.name = this.editCrmForm.get('name').value;
      crm.dic = this.editCrmForm.get('dic').value;
      crm.crmType = this.editCrmForm.get('crmType').value.split(':')[1].trim();
      crm.ico = this.editCrmForm.get('ico').value;
      crm.email = this.editCrmForm.get('email').value;
      crm.phone = this.editCrmForm.get('phone').value;
      crm.bankAccount = this.editCrmForm.get('bankAccount').value;
      crm.primaryAddress = new Address();
      crm.primaryAddress.street = this.editCrmForm.get('street').value;
      crm.primaryAddress.descNumber = this.editCrmForm.get('descNumber').value;
      crm.primaryAddress.city = this.editCrmForm.get('city').value;
      crm.primaryAddress.zipCode = this.editCrmForm.get('zipCode').value;
      if (!this.id) {
        this.crmsService.createCrm(crm).subscribe((resp) => {
          console.log('Crm saved.');
          this.router.navigate(['/crms']);
        });
      } else {
        this.crmsService.updateCrm(crm).subscribe((resp) => {
          console.log('Crm saved.');
          this.router.navigate(['/crms']);
        });
      }
    }
  }

  changeCrmType(e) {
    this.crmType.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get crmType() {
    return this.editCrmForm.get('crmType');
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}

