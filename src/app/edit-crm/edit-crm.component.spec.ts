import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCrmComponent } from './edit-crm.component';

describe('EditCrmComponent', () => {
  let component: EditCrmComponent;
  let fixture: ComponentFixture<EditCrmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCrmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCrmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
