import {Component, OnInit} from '@angular/core';
import {CrmItem, GlWriteFe, GlWriteFeBoth, GsItem} from '../models/gl-writes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlAccount} from '../models/accounts';
import {AccountService} from '../account.service';
import {GoodsServicesService} from '../goods-services.service';
import {CrmsService} from '../crms.service';
import {ActivatedRoute, Router} from '@angular/router';
import {formatDate} from '@angular/common';
import {GlWriteDto, WarehouseGoodsDto} from '../models/InputGoodsReceipt';
import {GoodsServices} from '../models/goods-services';
import {OutputInvoiceService} from '../output-invoice.service';
import {OutputGoodsReceipt, OutputInvoiceRequest} from '../models/invoices';
import {OutputGoodsReceiptService} from '../output-goods-receipt.service';

@Component({
  selector: 'app-output-invoice',
  templateUrl: './output-invoice.component.html',
  styleUrls: ['./output-invoice.component.css']
})
export class OutputInvoiceComponent implements OnInit {

  outputGoodsReceipts: OutputGoodsReceipt[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];


  rows: Array<GlWriteFeBoth> = [];

  accountingForm: FormGroup;

  identNext = 0;
  identGsNext = 0;

  griIds: Array<number> = [];

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];
  selectedRow: GlWriteFeBoth;
  // documentTypes=['InputInvoice', 'OutputInvoice', 'cash_output_receipt', 'cash_input_receipt', 'Warehouse_Income', 'Warehouse_Outcome'];

  goodsServices: Array<GoodsServices> = [];

  gsItems: Array<GsItem> = [];

  crm: CrmItem[];
  savedId: number;

  constructor(private formBuilder: FormBuilder, private accountService: AccountService, private goodsService: GoodsServicesService,
              private crmService: CrmsService, private activatedRoute: ActivatedRoute, private router: Router,
              private outputGoodsReceiptService: OutputGoodsReceiptService, private outputInvoiceService: OutputInvoiceService) {
    this.accountingForm = this.formBuilder.group({
      issueDate: ['', [Validators.required]],
      payDate: ['', [Validators.required]],
      crmCompany: ['', [Validators.required]],
      documentNumber: ['', [Validators.required]],
      paymentType: ['', [Validators.required]],
      transportType: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.goodsService.getGoodsServices(0, 100).subscribe(goodsServices => {
      this.goodsServices = goodsServices.goodsServices.content;
    });

    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.accountsDebit = accounts.accounts.content;
      this.accountsCredit = accounts.accounts.content;
    });

    this.accountingForm.get('issueDate').setValue(formatDate(new Date(), 'yyyy-MM-dd', 'en'), {onlySelf: true});

    this.crmService.getCrms(0, 1000).subscribe(data => {
      this.crm = data.crms.content;
    });
    this.readDataOutReceipt();

    this.outputInvoiceService.nextDocumentNumber().subscribe(resp => {
      this.accountingForm.get('documentNumber').setValue(resp.documentNumber, {
        onlySelf: true, emitEvent: false
      });
    });
  }

  private readDataOutReceipt() {
    this.outputInvoiceService.getOutputGoodsReceiptNotInvoiced(this.page, this.size).subscribe((outputGoodsReceiptsResponse) => {
      this.outputGoodsReceipts = outputGoodsReceiptsResponse.goodsOutputReceipts.content;
      this.page = outputGoodsReceiptsResponse.goodsOutputReceipts.number;
      this.size = outputGoodsReceiptsResponse.goodsOutputReceipts.size;
      this.total = outputGoodsReceiptsResponse.goodsOutputReceipts.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.readDataOutReceipt();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  addRow() {
    const newRow = new GlWriteFeBoth();
    newRow.ident = this.identNext++;
    newRow.debit = new GlWriteFe();

    newRow.credit = new GlWriteFe();
    this.rows.push(newRow);
  }

  deleteRow() {
    if (this.selectedRow) {
      this.rows = this.rows.filter(row => row.ident !== this.selectedRow.ident);
      this.selectedRow = null;
    }
  }

  selectRow(glWrite: GlWriteFeBoth) {
    if (this.selectedRow && this.selectedRow.ident === glWrite.ident) {
      this.selectedRow = null;
    } else {
      this.selectedRow = glWrite;
    }
  }

  submitAccountingForm() {
    const request = new OutputInvoiceRequest();
    const crmValue = this.accountingForm.get('crmCompany').value;
    request.issueDate = this.accountingForm.get('issueDate').value;
    request.payDate = this.accountingForm.get('payDate').value;
    request.paymentType = this.accountingForm.get('paymentType').value;
    request.transportType = this.accountingForm.get('transportType').value;
    request.GORIds = this.griIds;
    const splitedCrm = (crmValue as string).split(':');
    request.crmId = Number(splitedCrm[1]);
    request.documentNumber = this.accountingForm.get('documentNumber').value;
    const glWrites = [];
    const goods = [];
    this.rows.forEach(row => {
      if (row.debit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'DEBIT';
        glWriteDto.amount = row.debit.amount;
        glWriteDto.analyticPart = row.debit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.debit.acctNoWithDesc, this.accountsDebit).id;
        glWrites.push(glWriteDto);
      }
      if (row.credit.acctNoWithDesc) {
        const glWriteDto = new GlWriteDto();
        glWriteDto.glAccountSideType = 'CREDIT';
        glWriteDto.amount = row.credit.amount;
        glWriteDto.analyticPart = row.credit.analyticPart;
        glWriteDto.glAccountId = this.findAccountByAcctno(row.credit.acctNoWithDesc, this.accountsCredit).id;
        glWrites.push(glWriteDto);
      }
    });
    this.gsItems.forEach(gsItem => {
      const warehouseGoodsDto = new WarehouseGoodsDto();
      warehouseGoodsDto.count = gsItem.count;
      warehouseGoodsDto.warehouseId = gsItem.warehouseId;
      warehouseGoodsDto.goodsServiceId = gsItem.id;
      warehouseGoodsDto.unitPrice = this.getUnitPrice(gsItem.id);
      warehouseGoodsDto.vatTotal = this.getVat(gsItem.id, gsItem.count);
      warehouseGoodsDto.priceTotal = this.getPrice(gsItem.id, gsItem.count);
      goods.push(warehouseGoodsDto);
    });
    request.glWrites = glWrites;
    request.warehouseGoodsDtos = goods;
    console.log('REQUEST: '  + JSON.stringify(request));
    this.outputInvoiceService.createOutputInvoice(request).subscribe((resp) => {
      this.savedId = resp.id;
      // this.router.navigate(['/menu']);
    });
  }

  findAccountByAcctno(acctnoWithDesc: string, accounts: Array<GlAccount>): GlAccount {
    const strings = acctnoWithDesc.split('-');
    const acctno = strings[0].trim();
    const glAccounts = accounts.filter(account => account.acctNo === acctno);
    return glAccounts[0];
  }

  blurDesc($event: FocusEvent) {
    console.log('FocusEventType: ' + $event.type);
    console.log('FocusEvent: ' + this.accountingForm.get('description').value);
  }

  addRowGs() {
    const gsItem = new GsItem();
    gsItem.ident = this.identGsNext++;
    this.gsItems.push(gsItem);
  }

  // calcPrice(rowsElement: GsItem): number {
  //   const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
  //   if (goodsServices1.length === 1 && rowsElement.count) {
  //     return goodsServices1[0].packetInputPrice * rowsElement.count;
  //   } else {
  //     return 0;
  //   }
  // }
  //
  // calcVat(rowsElement: GsItem): number {
  //   const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
  //   if (goodsServices1.length === 1 && rowsElement.count) {
  //     return Math.round(goodsServices1[0].packetInputPrice * rowsElement.count * (goodsServices1[0].vatPercents / 100) * 100) / 100;
  //   } else {
  //     return 0;
  //   }
  // }
  //
  // calcTotal(rowsElement: GsItem) {
  //   const goodsServices1 = this.goodsServices.filter(gs => gs.id === Number(rowsElement.id));
  //   if (goodsServices1.length === 1 && rowsElement.count) {
  //     return Math.round(goodsServices1[0].packetInputPrice * rowsElement.count *
  //       ((100 + goodsServices1[0].vatPercents) / 100) * 100) / 100;
  //   } else {
  //     return 0;
  //   }
  // }

  calcTotalPrice(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getUnitPrice(item.id) * item.count;
    });
    return total;
  }

  calcTotalVat(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getVat(item.id, item.count);
    });
    return total;
  }

  calcTotalTotal(): number {
    let total = 0;
    this.gsItems.forEach(item => {
      total += this.getPrice(item.id, item.count);
    });
    return total;
  }

  delRowGs(rowsElement: GsItem) {
    this.gsItems = this.gsItems.filter(elem => elem.ident !== rowsElement.ident);
  }

  changeCompany(e) {
    this.crmCompany.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get crmCompany() {
    return this.accountingForm.get('crmCompany');
  }

  // getState(id: number) {
  //   if (id) {
  //     const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
  //     return whGoodsLocal[0].count;
  //   }
  //   return 0;
  // }

  getUnitPrice(id: number): number {
    if (id) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return whGoodsLocal[0].packetOutputPrice;
    }
    return 0;
  }

  getVat(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round((((whGoodsLocal[0].vatPercents / 100) *
        whGoodsLocal[0].packetOutputPrice) * count) * 100) / 100;
    }
    return 0;
  }

  getPrice(id: number, count: number): number {
    if (id && count) {
      const whGoodsLocal = this.goodsServices.filter(row => row.id === Number(id));
      return Math.round((whGoodsLocal[0].packetOutputPrice * count + this.getVat(id, count)) * 100) / 100;
    }
    return 0;
  }

  generateAccounting() {
    this.accountService.getAccountsByGroup('TRADE_OTHER_RECEIVABLES').subscribe(tar => {
      console.log('Goods account TAP: ' + tar[0].acctNo);
      const tradeOthetReceivablesAcctno = tar[0].acctNo;
      this.accountService.getAccountsByGroup('REVENUE_GOODS').subscribe(rg => {
        console.log('Goods revenue account get: ' + rg[0].acctNo);
        const accountRevenueGoodsAcctno = rg[0].acctNo;
        const glAccTar = this.getGlAccountByAcctno(tradeOthetReceivablesAcctno);
        const glAccRg = this.getGlAccountByAcctno(accountRevenueGoodsAcctno);
        const amountWoVat = this.calcTotalPrice();
        const vatTotal = this.calcTotalVat();
        const amount = this.calcTotalTotal();
        this.rows = [];
        this.addRow();
        this.rows[0].credit.acctNoWithDesc = glAccRg.acctNo + '-' + glAccRg.description;
        this.rows[0].credit.amount = amountWoVat;
        this.rows[0].credit.analyticPart = String('000');
        this.rows[0].debit.acctNoWithDesc = glAccTar.acctNo + '-' + glAccTar.description;
        this.rows[0].debit.amount = amount;
        this.rows[0].debit.analyticPart = String('000');


        this.accountService.getAccountsByGroup('VAT').subscribe(vat => {
          console.log('vat account: ' + vat[0].acctNo);
          const accountVatAcctno = vat[0].acctNo;
          const glAccVat = this.getGlAccountByAcctno(accountVatAcctno);

          this.addRow();
          this.rows[1].credit.acctNoWithDesc = glAccVat.acctNo + '-' + glAccVat.description;
          this.rows[1].credit.amount = vatTotal;
          this.rows[1].credit.analyticPart = String('000');
        });
      });
    });
  }

  getGlAccountByAcctno(acctno: string): GlAccount {
    const account = this.accountsDebit.filter(acc => acc.acctNo === acctno);
    return account[0];
  }

  toggle(outputGoodsReceipt: OutputGoodsReceipt) {
    this.griIds.push(outputGoodsReceipt.id);
    outputGoodsReceipt.selected = !outputGoodsReceipt.selected;
    if (outputGoodsReceipt.selected) {
      outputGoodsReceipt.goodsServiceItems.forEach(itemOrig => {
        const gsItem = new GsItem();
        gsItem.id = itemOrig.warehouseGoodsServices.goodsServices.id;
        gsItem.name = itemOrig.warehouseGoodsServices.goodsServices.name;
        gsItem.count = itemOrig.numberOfUnits;
        gsItem.warehouseId = itemOrig.warehouseGoodsServices?.warehouse?.id;
        gsItem.ident = this.identGsNext++;
        this.gsItems.push(gsItem);
      });
    } else {
      this.gsItems = [];
      this.griIds = [];
    }
  }

  generatePdf(id: number) {
    this.outputInvoiceService.getPdf(id).subscribe(responseMessage => {
      const file = new Blob([responseMessage], { type: 'application/pdf' });
      const fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    });
  }
}
