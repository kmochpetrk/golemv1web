import { TestBed } from '@angular/core/testing';

import { CommandToPayService } from './command-to-pay.service';

describe('CommandToPayService', () => {
  let service: CommandToPayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommandToPayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
