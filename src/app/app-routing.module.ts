import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {GlAccountsComponent} from './gl-accounts/gl-accounts.component';
import {MenuComponent} from './menu/menu.component';
import {EditGlaccountComponent} from './edit-glaccount/edit-glaccount.component';
import {GoodsServicesComponent} from './goods-services/goods-services.component';
import {EditGoodsServicesComponent} from './edit-goods-services/edit-goods-services.component';
import {GeneralAccountingComponent} from './general-accounting/general-accounting.component';
import {CrmsComponent} from './crms/crms.component';
import {EditCrmComponent} from './edit-crm/edit-crm.component';
import {WarehousesComponent} from './warehouses/warehouses.component';
import {WhGoodsComponent} from './wh-goods/wh-goods.component';
import {InputGoodsReceiptComponent} from './input-goods-receipt/input-goods-receipt.component';
import {OutputGoodsReceiptComponent} from './output-goods-receipt/output-goods-receipt.component';
import {OutputInvoiceComponent} from './output-invoice/output-invoice.component';
import {InputInvoiceComponent} from './input-invoice/input-invoice.component';
import {EshopProductComponent} from './eshop/eshop-product/eshop-product.component';
import {EshopCheckoutComponent} from './eshop/eshop-checkout/eshop-checkout.component';
import {BookingsComponent} from './bookings/bookings.component';
import {BankStatementIncomeComponent} from './bank-statement-income/bank-statement-income.component';
import {BankStatementOutcomeComponent} from './bank-statement-outcome/bank-statement-outcome.component';
import {InputCashReceiptComponent} from './input-cash-receipt/input-cash-receipt.component';
import {OutputCashReceiptComponent} from './output-cash-receipt/output-cash-receipt.component';
import {BalanceSheetComponent} from './balance-sheet/balance-sheet.component';
import {ProfitLossComponent} from './profit-loss/profit-loss.component';
import {InternalOrderComponent} from './internal-order/internal-order.component';
import {CommandToPayComponent} from './command-to-pay/command-to-pay.component';
import {CommandToPayListComponent} from './command-to-pay-list/command-to-pay-list.component';
import {CreateCompanyComponent} from './create-company/create-company.component';
import {CreateWarehouseComponent} from './create-warehouse/create-warehouse.component';
import {InventoryComponent} from './inventory/inventory.component';
import {InventoryListComponent} from './inventory-list/inventory-list.component';
import {EmployeeCreateComponent} from './employee-create/employee-create.component';
import {EmployeeListComponent} from './employee-list/employee-list.component';
import {InputInvoiceNotPaidComponent} from './input-invoice-not-paid/input-invoice-not-paid.component';
import {OutputInvoiceNotPaidComponent} from './output-invoice-not-paid/output-invoice-not-paid.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'glaccounts', component: GlAccountsComponent},
  { path: 'edit-glaccount', component: EditGlaccountComponent},
  { path: 'edit-glaccount/:id', component: EditGlaccountComponent},
  { path: 'menu', component: MenuComponent},
  { path: 'goods-services', component: GoodsServicesComponent},
  { path: 'edit-goods-services', component: EditGoodsServicesComponent},
  { path: 'edit-goods-services/:id', component: EditGoodsServicesComponent},
  { path: 'general-accounting', component: GeneralAccountingComponent},
  { path: 'crms', component: CrmsComponent},
  { path: 'edit-crm', component: EditCrmComponent},
  { path: 'edit-crm/:id', component: EditCrmComponent},
  { path: 'warehouses', component: WarehousesComponent},
  { path: 'warehouse-goods/:id/:warehouse', component: WhGoodsComponent},
  { path: 'edit-wh-goods/:warehouseId/:warehouseName', component: InputGoodsReceiptComponent},
  { path: 'edit-wh-goods-out/:warehouseId/:warehouseName', component: OutputGoodsReceiptComponent},
  { path: 'output-invoice', component: OutputInvoiceComponent},
  { path: 'input-invoice', component: InputInvoiceComponent},
  { path: 'eshop/:company', component: EshopProductComponent},
  { path: 'eshop/:company/eshop-checkout', component: EshopCheckoutComponent},
  { path: 'bookings', component: BookingsComponent},
  { path: 'bank-statement-income', component: BankStatementIncomeComponent},
  { path: 'bank-statement-outcome', component: BankStatementOutcomeComponent},
  { path: 'cash-input-receipt', component: InputCashReceiptComponent},
  { path: 'cash-output-receipt', component: OutputCashReceiptComponent},
  { path: 'balance-sheet', component: BalanceSheetComponent},
  { path: 'profit-loss', component: ProfitLossComponent},
  { path: 'internal-order', component: InternalOrderComponent},
  { path: 'command-to-pay', component: CommandToPayComponent},
  { path: 'command-to-pay-list', component: CommandToPayListComponent},
  { path: 'create-company', component: CreateCompanyComponent},
  { path: 'edit-warehouse', component: CreateWarehouseComponent},
  { path: 'edit-warehouse/:id', component: CreateWarehouseComponent},
  { path: 'inventory', component: InventoryComponent},
  { path: 'inventory-list', component: InventoryListComponent},
  { path: 'employee-create', component: EmployeeCreateComponent},
  { path: 'employee-list', component: EmployeeListComponent},
  { path: 'input-invoices-not-paid', component: InputInvoiceNotPaidComponent},
  { path: 'output-invoices-not-paid', component: OutputInvoiceNotPaidComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
