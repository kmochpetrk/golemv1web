import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {GoodsServicesService} from '../goods-services.service';
import {Subscription} from 'rxjs';
import {GoodsServices} from '../models/goods-services';

@Component({
  selector: 'app-edit-goods-services',
  templateUrl: './edit-goods-services.component.html',
  styleUrls: ['./edit-goods-services.component.css']
})
export class EditGoodsServicesComponent implements OnInit, OnDestroy {

  editGoodsServicesForm: FormGroup;
  readGoodsServices: GoodsServices;
  private sub: Subscription;
  id: number;
  gsTypes = ['GOODS', 'SERVICE', 'OTHER']

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute,
              private goodsServicesService: GoodsServicesService, private router: Router) {
    this.editGoodsServicesForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      unit: ['', [Validators.required]],
      packetOutputPrice: [0, [Validators.required]],
      packetInputPrice: [0, [Validators.required]],
      vatPercents: [0, [Validators.required]],
      numberOfUnitsInPacket: [0, [Validators.required]],
      gsType: [0, [Validators.required]],
      documentNumber: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params.id;
      if (this.id) {
        this.goodsServicesService.getGoodsServicesById(this.id).subscribe((goodsServices) => {
          this.readGoodsServices = goodsServices;
          this.editGoodsServicesForm.get('name').setValue(goodsServices.name, {
            onlySelf: true, emitEvent: false
          });
          this.editGoodsServicesForm.get('unit').setValue(goodsServices.unit, {
            onlySelf: true, emitEvent: false
          });
          this.editGoodsServicesForm.get('packetOutputPrice').setValue(goodsServices.packetOutputPrice, {
            onlySelf: true, emitEvent: false
          });
          this.editGoodsServicesForm.get('packetInputPrice').setValue(goodsServices.packetInputPrice, {
            onlySelf: true, emitEvent: false
          });
          this.editGoodsServicesForm.get('vatPercents').setValue(goodsServices.vatPercents, {
            onlySelf: true, emitEvent: false
          });
          this.editGoodsServicesForm.get('numberOfUnitsInPacket').setValue(goodsServices.numberOfUnitsInPacket, {
            onlySelf: true, emitEvent: false
          });
          this.editGoodsServicesForm.get('documentNumber').setValue(goodsServices.goodsServicesNumber, {
            onlySelf: true, emitEvent: false
          });
          const gsType = this.gsTypes.indexOf(goodsServices.gsType) +
            ': ' + goodsServices.gsType;
          this.editGoodsServicesForm.get('gsType').setValue(gsType, {
            onlySelf: true, emitEvent: false
          });
        });
      } else {
        this.goodsServicesService.nextDocumentNumber().subscribe(resp => {
          this.editGoodsServicesForm.get('documentNumber').setValue(resp.documentNumber, {
            onlySelf: true, emitEvent: false
          });
        });
      }
    });
  }

  submitGoodsServicesForm() {
    this.editGoodsServicesForm.updateValueAndValidity();
    if (this.editGoodsServicesForm.valid) {
      const goodsServices = new GoodsServices();
      if (this.id) {
        goodsServices.id = this.id;
      }
      goodsServices.name = this.editGoodsServicesForm.get('name').value;
      goodsServices.unit = this.editGoodsServicesForm.get('unit').value;
      goodsServices.goodsServicesNumber = this.editGoodsServicesForm.get('documentNumber').value;
      goodsServices.packetInputPrice = this.editGoodsServicesForm.get('packetInputPrice').value;
      goodsServices.packetOutputPrice = this.editGoodsServicesForm.get('packetOutputPrice').value;
      goodsServices.vatPercents = this.editGoodsServicesForm.get('vatPercents').value;
      goodsServices.vatPercents = this.editGoodsServicesForm.get('vatPercents').value;
      goodsServices.numberOfUnitsInPacket = this.editGoodsServicesForm.get('numberOfUnitsInPacket').value;
      goodsServices.gsType = this.editGoodsServicesForm.get('gsType').value.split(':')[1].trim();
      if (!this.id) {
        this.goodsServicesService.createGoodsServices(goodsServices).subscribe((resp) => {
          console.log('Account saved.');
          this.router.navigate(['/goods-services']);
        });
      } else {
        this.goodsServicesService.updateGoodsServices(goodsServices).subscribe((resp) => {
          console.log('Account saved.');
          this.router.navigate(['/goods-services']);
        });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  get gsType() {
    return this.editGoodsServicesForm.get('gsType');
  }


  changeGsType(e) {
    this.gsType.setValue(e.target.value, {
      onlySelf: true
    });
  }
}
