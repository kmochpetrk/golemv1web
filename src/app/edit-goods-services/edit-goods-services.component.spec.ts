import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGoodsServicesComponent } from './edit-goods-services.component';

describe('EditGoodsServicesComponent', () => {
  let component: EditGoodsServicesComponent;
  let fixture: ComponentFixture<EditGoodsServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGoodsServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGoodsServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
