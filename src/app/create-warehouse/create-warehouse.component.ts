import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Address, Crm} from '../models/crms';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {CrmsService} from '../crms.service';
import {Warehouse} from '../models/warehouses';
import {WarehouseService} from '../warehouse.service';

@Component({
  selector: 'app-create-warehouse',
  templateUrl: './create-warehouse.component.html',
  styleUrls: ['./create-warehouse.component.css']
})
export class CreateWarehouseComponent implements OnInit, OnDestroy {

  editCrmForm: FormGroup;
  private sub: Subscription;
  id: number;

  constructor(private formBuilder: FormBuilder, public activatedRoute: ActivatedRoute,
              private warehouseService: WarehouseService, private router: Router) {
    this.editCrmForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      street: ['', [Validators.required]],
      descNumber: ['', [Validators.required]],
      city: ['', [Validators.required]],
      zipCode: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params.id;
      if (this.id) {
        this.warehouseService.getWarehouseById(this.id).subscribe((wh) => {
          this.editCrmForm.get('name').setValue(wh.name, {
            onlySelf: true, emitEvent: false
          });
          if (wh.address) {
            this.editCrmForm.get('street').setValue(wh.address.street, {
              onlySelf: true, emitEvent: false
            });
            this.editCrmForm.get('descNumber').setValue(wh.address.descNumber, {
              onlySelf: true, emitEvent: false
            });
            this.editCrmForm.get('city').setValue(wh.address.city, {
              onlySelf: true, emitEvent: false
            });
            this.editCrmForm.get('zipCode').setValue(wh.address.zipCode, {
              onlySelf: true, emitEvent: false
            });
          }
        });
      }
    });
  }

  submitWarehouseForm() {
    this.editCrmForm.updateValueAndValidity();
    if (this.editCrmForm.valid) {
      const warehouse = new Warehouse();
      warehouse.name = this.editCrmForm.get('name').value;
      warehouse.address = new Address();
      warehouse.address.street = this.editCrmForm.get('street').value;
      warehouse.address.descNumber = this.editCrmForm.get('descNumber').value;
      warehouse.address.city = this.editCrmForm.get('city').value;
      warehouse.address.zipCode = this.editCrmForm.get('zipCode').value;
      if (!this.id) {
        this.warehouseService.createWarehouse(warehouse).subscribe((resp) => {
          console.log('wh saved.');
          this.router.navigate(['/warehouses']);
        });
      } else {
        this.warehouseService.updateWarehouse(warehouse).subscribe((resp) => {
          console.log('wh saved.');
          this.router.navigate(['/warehouses']);
        });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
