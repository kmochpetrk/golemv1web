import { Component, OnInit } from '@angular/core';
import {InputInvoiceNotPaid, OutputInvoiceNotPaid} from '../models/accounts';
import {BookingsService} from '../bookings.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-output-invoice-not-paid',
  templateUrl: './output-invoice-not-paid.component.html',
  styleUrls: ['./output-invoice-not-paid.component.css']
})
export class OutputInvoiceNotPaidComponent implements OnInit {
  outputInvoicesNotPaid: OutputInvoiceNotPaid[];
  selectedOutputInvoiceNotPaid: InputInvoiceNotPaid;
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  constructor(private bookingsService: BookingsService, private router: Router) { }

  ngOnInit(): void {
    this.bookingsService.getOutputInvoicesNotPaid(this.page, this.size).subscribe(outputInvoiceNotPaidResponse => {
      this.outputInvoicesNotPaid = outputInvoiceNotPaidResponse.outputMediumInvoices.content;
      this.page = outputInvoiceNotPaidResponse.outputMediumInvoices.number;
      this.size = outputInvoiceNotPaidResponse.outputMediumInvoices.size;
      this.total = outputInvoiceNotPaidResponse.outputMediumInvoices.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
}

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  selectOutputInvoiceNotPaid(outputInvoiceNotPaid: OutputInvoiceNotPaid) {
    this.selectedOutputInvoiceNotPaid = outputInvoiceNotPaid;
  }
}
