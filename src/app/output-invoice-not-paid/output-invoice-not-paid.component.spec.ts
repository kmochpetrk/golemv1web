import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputInvoiceNotPaidComponent } from './output-invoice-not-paid.component';

describe('OutputInvoiceNotPaidComponent', () => {
  let component: OutputInvoiceNotPaidComponent;
  let fixture: ComponentFixture<OutputInvoiceNotPaidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputInvoiceNotPaidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputInvoiceNotPaidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
