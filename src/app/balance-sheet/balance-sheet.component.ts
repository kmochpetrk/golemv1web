import { Component, OnInit } from '@angular/core';
import {BalanceSheetItem, GlAccount} from '../models/accounts';
import {AuthenticationService} from '../authentication.service';
import {AccountService} from '../account.service';
import {Router} from '@angular/router';
import {AccountingStatementsService} from '../accounting-statements.service';

@Component({
  selector: 'app-balance-sheet',
  templateUrl: './balance-sheet.component.html',
  styleUrls: ['./balance-sheet.component.css']
})
export class BalanceSheetComponent implements OnInit {
  balanceSheetItems: BalanceSheetItem[];

  constructor(public authenticationService: AuthenticationService,
              private accountingStatementsService: AccountingStatementsService, private router: Router) { }

  ngOnInit(): void {
    this.accountingStatementsService.getBalanceSheet().subscribe(accountsResponse => {
      this.balanceSheetItems = accountsResponse.balance;
    });
  }
}
