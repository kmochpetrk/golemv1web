import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {ComplexMonthlySalary, Employee} from './models/crms';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {NextNumberResponse} from './models/NextNumberResponse';
import {EmployeesResponse} from './models/accounts';

@Injectable()
export class EmployeeService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public createEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(environment.baseUrl +
      '/employee', employee,  this.authService.createOptions());
  }

  public nextDocumentNumber(): Observable<NextNumberResponse> {
    return this.http.get<NextNumberResponse>(environment.baseUrl +
      '/employee/nextnumber', this.authService.createOptions());
  }

  public getEmployeeItems(page: number, size: number): Observable<EmployeesResponse> {
    return this.http.get<EmployeesResponse>(environment.baseUrl +
      '/employee?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public createMonthlySalaryItem(complexMonthlySalary: ComplexMonthlySalary): Observable<Employee> {
    return this.http.post<Employee>(environment.baseUrl +
      '/employee/monthly-salary', complexMonthlySalary,  this.authService.createOptions());
  }
}
