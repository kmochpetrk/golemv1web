import { TestBed } from '@angular/core/testing';

import { AccountingStatementsService } from './accounting-statements.service';

describe('AccountingStatementsService', () => {
  let service: AccountingStatementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountingStatementsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
