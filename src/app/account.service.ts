import { Injectable } from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AccountsResponse, GlAccount} from './models/accounts';
import {environment} from '../environments/environment';
import {GeneralAccountingPredictRequest, GeneralAccountingTrainRequest} from './models/general-accounting-train-request';
import {GeneralAccountingPredictResponse, GeneralAccountingTrainResponse} from './models/general-accounting-train-response';

@Injectable()
export class AccountService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getAccounts(page: number, size: number): Observable<AccountsResponse> {
    return this.http.get<AccountsResponse>(environment.baseUrl +
      '/glaccount?page=' + page + '&size=' + size, this.authService.createOptions());
  }

  public createAccount(glAccount: GlAccount): Observable<GlAccount> {
    return this.http.post<GlAccount>(environment.baseUrl +
      '/glaccount', glAccount, this.authService.createOptions());
  }

  public updateAccount(glAccount: GlAccount): Observable<GlAccount> {
    return this.http.put<GlAccount>(environment.baseUrl +
      '/glaccount/' + glAccount.id, glAccount, this.authService.createOptions());
  }

  public getAccountById(id: number): Observable<GlAccount> {
    return this.http.get<GlAccount>(environment.baseUrl +
      '/glaccount/' + id, this.authService.createOptions());
  }

  public deleteAccountById(id: number): Observable<GlAccount> {
    return this.http.delete<GlAccount>(environment.baseUrl +
      '/glaccount/' + id, this.authService.createOptions());
  }

  getGroups(accType: string, side: string): Observable<Array<string>> {
    return this.http.get<Array<string>>(environment.baseUrl +
      '/glaccount/groups?type=' + accType + '&side=' + side,
      this.authService.createOptions());
  }

  public postTrain(generalAccountingTrainRequest: GeneralAccountingTrainRequest): Observable<GeneralAccountingTrainResponse> {
    return this.http.post<GeneralAccountingTrainResponse>(environment.baseUrl + '/general_accounting/train',
      generalAccountingTrainRequest, this.authService.createOptions());
  }

  public postPredict(generalAccountingTrainRequest: GeneralAccountingPredictRequest): Observable<GeneralAccountingPredictResponse> {
    return this.http.post<GeneralAccountingPredictResponse>(environment.baseUrl + '/general_accounting/predict',
      generalAccountingTrainRequest, this.authService.createOptions());
  }

  public getAccountsByGroup(group: string): Observable<GlAccount[]> {
    return this.http.get<GlAccount[]>(environment.baseUrl + '/glaccount/bygroup/' + group, this.authService.createOptions());
  }
}
