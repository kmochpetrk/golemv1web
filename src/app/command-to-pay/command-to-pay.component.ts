import {Component, OnInit} from '@angular/core';
import {InputInvoiceSmall} from '../models/invoices';
import {CrmItem, GlWriteFeBoth} from '../models/gl-writes';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommandToPay, CommandToPayItem, GlAccount} from '../models/accounts';
import {GoodsServices} from '../models/goods-services';
import {AccountService} from '../account.service';
import {CrmsService} from '../crms.service';
import {ActivatedRoute, Router} from '@angular/router';
import {InputInvoiceService} from '../input-invoice.service';
import {formatDate} from '@angular/common';
import {CommandToPayService} from '../command-to-pay.service';

@Component({
  selector: 'app-command-to-pay',
  templateUrl: './command-to-pay.component.html',
  styleUrls: ['./command-to-pay.component.css']
})
export class CommandToPayComponent implements OnInit {

  inputInvoiceRequests: InputInvoiceSmall[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];
  rows: Array<GlWriteFeBoth> = [];
  accountingForm: FormGroup;
  identGsNext = 0;

  griIds: Array<number> = [];

  accountsDebit: Array<GlAccount> = [];
  accountsCredit: Array<GlAccount> = [];
  selectedRow: GlWriteFeBoth;
  // documentTypes=['InputInvoice', 'OutputInvoice', 'cash_output_receipt', 'cash_input_receipt', 'Warehouse_Income', 'Warehouse_Outcome'];

  goodsServices: Array<GoodsServices> = [];

  commandToPayItems: Array<CommandToPayItem> = [];

  crm: CrmItem[];
  bankAccounts: string[];

  constructor(private formBuilder: FormBuilder, private accountService: AccountService,
              private crmService: CrmsService, private activatedRoute: ActivatedRoute, private router: Router,
              private commandToPayService: CommandToPayService, private inputInvoiceService: InputInvoiceService) {
    this.accountingForm = this.formBuilder.group({
      documentNumber: ['', [Validators.required]],
      ourBankAccount: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {

    this.accountService.getAccounts(0, 100).subscribe(accounts => {
      this.bankAccounts = accounts.accounts.content.filter(acc => acc.glAccountGroupingType === 'BANK_ACCOUNTS').map(u => u.additionalInfo);
    });
    this.readDataInpuInvoices();

    this.commandToPayService.nextDocumentNumber().subscribe(resp => {
      this.accountingForm.get('documentNumber').setValue(resp.documentNumber, {
        onlySelf: true, emitEvent: false
      });
    });
  }

  private readDataInpuInvoices() {
    this.inputInvoiceService.getInputInvoicesSmall(this.page, this.size).subscribe((inputInvoicesSmallResponse) => {
      this.inputInvoiceRequests = inputInvoicesSmallResponse.inputSmallInvoices.content;
      this.page = inputInvoicesSmallResponse.inputSmallInvoices.number;
      this.size = inputInvoicesSmallResponse.inputSmallInvoices.size;
      this.total = inputInvoicesSmallResponse.inputSmallInvoices.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.readDataInpuInvoices();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  submitAccountingForm() {
    const request = new CommandToPay();
    request.documentNumber = this.accountingForm.get('documentNumber').value;
    request.ourBankAccount = this.accountingForm.get('ourBankAccount').value.split(':')[1].trim();
    request.commandToPayItems = this.commandToPayItems;
    console.log('REQUEST: '  + JSON.stringify(request));
    this.commandToPayService.createCommandToPay(request).subscribe((resp) => {
      this.router.navigate(['/menu']);
    });
  }

  blurDesc($event: FocusEvent) {
    console.log('FocusEventType: ' + $event.type);
    console.log('FocusEvent: ' + this.accountingForm.get('description').value);
  }

  addRowCommandItem() {
    const commandToPayItem = new CommandToPayItem();
    commandToPayItem.ident = this.identGsNext++;
    this.commandToPayItems.push(commandToPayItem);
  }

  delRowGs(rowsElement: CommandToPayItem) {
    this.commandToPayItems = this.commandToPayItems.filter(elem => elem.ident !== rowsElement.ident);
  }

  getGlAccountByAcctno(acctno: string): GlAccount {
    const account = this.accountsDebit.filter(acc => acc.acctNo === acctno);
    return account[0];
  }

  toggle(inputInvoiceSmall: InputInvoiceSmall) {
    this.commandToPayItems = [];
    this.inputInvoiceRequests.forEach(a => a.selected = false);
    inputInvoiceSmall.selected = true;
    const commandToPayItem = new CommandToPayItem();
    commandToPayItem.ident = this.identGsNext++;
    commandToPayItem.variableSymbol = inputInvoiceSmall.variableSymbol;
    commandToPayItem.amount = inputInvoiceSmall.amount;
    commandToPayItem.otherBankAccount = inputInvoiceSmall.otherBankAccount;
    commandToPayItem.id = inputInvoiceSmall.id;
    this.commandToPayItems.push(commandToPayItem);
  }

  changeBankAccount(e) {
    this.ourBankAccount.setValue(e.target.value, {
      onlySelf: true
    });
  }

  get ourBankAccount() {
    return this.accountingForm.get('ourBankAccount');
  }
}
