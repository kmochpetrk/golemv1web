import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandToPayComponent } from './command-to-pay.component';

describe('CommandToPayComponent', () => {
  let component: CommandToPayComponent;
  let fixture: ComponentFixture<CommandToPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandToPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandToPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
