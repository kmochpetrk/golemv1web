import { TestBed } from '@angular/core/testing';

import { InputGoodsReceiptService } from './input-goods-receipt.service';

describe('InputGoodsReceiptService', () => {
  let service: InputGoodsReceiptService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InputGoodsReceiptService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
