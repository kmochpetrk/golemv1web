import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';
import {BalanceSheetResponse, ProfitLossResponse} from './models/accounts';

@Injectable()
export class AccountingStatementsService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getBalanceSheet(): Observable<BalanceSheetResponse> {
    return this.http.get<BalanceSheetResponse>(environment.baseUrl +
      '/balance-sheet',  this.authService.createOptions());
  }

  public getProfitLoss(): Observable<ProfitLossResponse> {
    return this.http.get<ProfitLossResponse>(environment.baseUrl +
      '/profit-loss',  this.authService.createOptions());
  }
}
