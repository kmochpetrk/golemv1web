import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputInvoiceNotPaidComponent } from './input-invoice-not-paid.component';

describe('InputInvoiceNotPaidComponent', () => {
  let component: InputInvoiceNotPaidComponent;
  let fixture: ComponentFixture<InputInvoiceNotPaidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputInvoiceNotPaidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputInvoiceNotPaidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
