import {Component, OnInit} from '@angular/core';
import {Booking} from '../models/accounts';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';
import {BookingsService} from '../bookings.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
  bookings: Booking[];
  selectedBooking: Booking;
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];
  documentTypes: ['ALL', 'INPUT_INVOICE', 'OUTPUT_INVOICE', 'BANK_STATEMENT', 'CASH_INPUT_RECEIPT', 'CASH_OUTPUT_RECEIPT',
    'GOODS_INPUT_RECEIPT', 'GOODS_OUTPUT_RECEIPT', 'INTERNAL'];
  selectedDocumentType = 'ALL';

  constructor(public authenticationService: AuthenticationService,
              private bookingsService: BookingsService, private router: Router) { }

  ngOnInit(): void {
    this.bookingsService.getBookings(this.page, this.size, this.selectedDocumentType).subscribe(bookingsResponse => {
      this.bookings = bookingsResponse.bookings.content;
      this.page = bookingsResponse.bookings.number;
      this.size = bookingsResponse.bookings.size;
      this.total = bookingsResponse.bookings.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  selectBooking(booking: Booking) {
    this.selectedBooking = booking;
  }

  cancel(booking: Booking) {
    this.bookingsService.cancel(booking).subscribe (resp => {
      booking.cancelled = true;
    });
  }

  changeSelectedDocType($event: Event) {
    console.log('Change docType: ' + this.selectedDocumentType);
    this.page = 0;
    this.size = 5;
    this.ngOnInit();
  }
}
