import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputCashReceiptComponent } from './output-cash-receipt.component';

describe('OutputCashReceiptComponent', () => {
  let component: OutputCashReceiptComponent;
  let fixture: ComponentFixture<OutputCashReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputCashReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputCashReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
