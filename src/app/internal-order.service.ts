import {Injectable} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {InternalOrdersResponse} from './models/accounts';
import {environment} from '../environments/environment';

@Injectable()
export class InternalOrderService {

  constructor(private authService: AuthenticationService, private http: HttpClient) { }

  public getInternalOrders(page: number, size: number): Observable<InternalOrdersResponse> {
    return this.http.get<InternalOrdersResponse>(environment.baseUrl +
      '/internal-orders?page=' + page + '&size=' + size, this.authService.createOptions());
  }
}
