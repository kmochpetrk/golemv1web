import { Component, OnInit } from '@angular/core';
import {BalanceSheetItem} from '../models/accounts';
import {AuthenticationService} from '../authentication.service';
import {AccountingStatementsService} from '../accounting-statements.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profit-loss',
  templateUrl: './profit-loss.component.html',
  styleUrls: ['./profit-loss.component.css']
})
export class ProfitLossComponent implements OnInit {
  balanceSheetItems: BalanceSheetItem[];

  constructor(public authenticationService: AuthenticationService,
              private accountingStatementsService: AccountingStatementsService, private router: Router) { }

  ngOnInit(): void {
    this.accountingStatementsService.getProfitLoss().subscribe(accountsResponse => {
      this.balanceSheetItems = accountsResponse.profitAndLoss;
    });
  }
}
