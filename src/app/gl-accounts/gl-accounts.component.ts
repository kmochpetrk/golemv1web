import { Component, OnInit } from '@angular/core';
import {GlAccount} from '../models/accounts';
import {AuthenticationService} from '../authentication.service';
import {AccountService} from '../account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gl-accounts',
  templateUrl: './gl-accounts.component.html',
  styleUrls: ['./gl-accounts.component.css']
})
export class GlAccountsComponent implements OnInit {
  accounts: GlAccount[];
  page = 0;
  size = 5;
  total: number;
  pageNumbers: number[];

  constructor(public authenticationService: AuthenticationService,
              private accountsService: AccountService, private router: Router) { }

  ngOnInit(): void {
    this.accountsService.getAccounts(this.page, this.size).subscribe(accountsResponse => {
      this.accounts = accountsResponse.accounts.content;
      this.page = accountsResponse.accounts.number;
      this.size = accountsResponse.accounts.size;
      this.total = accountsResponse.accounts.totalElements;
      const sizes = Math.ceil(this.total / this.size);
      console.log('sizes ' + sizes);
      const pageNumbers = [];
      if (this.page > 0) {
        pageNumbers.push(this.page);
        // pageNumbers.push(page + 1);
        if (sizes > 1 && (((this.page + 1) < sizes))) {
          pageNumbers.push(this.page + 1);
        }
        // if (this.page < (sizes - 1)) {
        //   pageNumbers.push(page + 2);
        // }
      } else {
        if (sizes >= 0) {
          pageNumbers.push(0);
        }
        if (sizes >= 1) {
          pageNumbers.push(1);
        }
      }
      this.pageNumbers = pageNumbers;
    });
  }

  readData(page: number, size: number) {
    this.page = page;
    this.size = size;
    this.ngOnInit();
  }

  ceil(total: number, size: number) {
    return Math.floor(total / size) - (((total % size) !== 0) ? 0 : 1);
  }

  editGlAccount(account: GlAccount) {
    this.router.navigate(['/edit-glaccount', account.id]);
    // this.accountsService.updateAccount(account).subscribe((resp) => {
    //   console.log('edit account');
    //   this.ngOnInit();
    // });
  }

  createGlAccount() {
    this.router.navigate(['/edit-glaccount']);
  }

  deleteGlAccount(account: GlAccount) {
    this.accountsService.deleteAccountById(account.id).subscribe((resp) => {
      console.log('delete account');
      this.page = 0;
      this.ngOnInit();
    });
  }
}
