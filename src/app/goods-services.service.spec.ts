import { TestBed } from '@angular/core/testing';

import { GoodsServicesService } from './goods-services.service';

describe('GoodsServicesService', () => {
  let service: GoodsServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoodsServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
